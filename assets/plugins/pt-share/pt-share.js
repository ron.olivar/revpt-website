/*!
 * Name    : pt Share
 * Version : 1.0.0
 * Author  : _pt http://ptdev.info
 */
(function(factory) {
    'use strict';
    if (typeof define === 'function' && define.amd) {
        define(['jquery'], factory);
    } else if (typeof exports !== 'undefined') {
        module.exports = factory(require('jquery'));
    } else {
        factory(jQuery);
    }
}(function($) {

    // ptShare instance
    var ptShare = (function() {
        var instanceID = 0;

        function ptShare(item, userOptions) {
            var _this = this;

            _this.$item      = $(item);

            _this.defaults   = {
                name          : null,
                text          : null,
                lipt          : null,
                media         : null,
                popupOptions  : 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600',
                networks      : {
                    facebook: function() {
                        var result = '//www.facebook.com/share.php?m2w&s=100&p[url]=' + encodeURIComponent(this.options.lipt);

                        if (this.options.media) {
                            result += '&p[images][0]=' + encodeURIComponent(this.options.media);
                        }
                        if (this.options.text) {
                            result += '&p[title]=' + encodeURIComponent(this.options.text);
                        }

                        window.open(result, 'Facebook', this.options.popupOptions);
                    },
                    twitter: function() {
                        var result = 'https://twitter.com/intent/tweet?original_referer=' + encodeURIComponent(this.options.lipt);

                        if (this.options.text) {
                            result += '&text=' + encodeURIComponent(this.options.text) + '%20' + encodeURIComponent(this.options.lipt);
                        } else {
                            result += '&text=' + encodeURIComponent(this.options.lipt);
                        }

                        window.open(result, 'Twitter', this.options.popupOptions);
                    },
                    pinterest: function() {
                        var result = '//pinterest.com/pin/create/button/?url=' + encodeURIComponent(this.options.lipt);

                        if (this.options.media) {
                            result += '&media=' + encodeURIComponent(this.options.media);
                        }
                        if (this.options.text) {
                            result += '&description=' + encodeURIComponent(this.options.text);
                        }

                        window.open(result, 'Pinterest', this.options.popupOptions);
                    },
                    'google-plus': function() {
                        window.open('//plus.google.com/share?url=' + encodeURIComponent(this.options.lipt), 'GooglePlus', this.options.popupOptions);
                    },
                    liptedin: function() {
                        var result = '//www.liptedin.com/shareArticle?mini=true&url=' + encodeURIComponent(this.options.lipt) + '&source=' + encodeURIComponent(this.options.lipt);

                        if (this.options.text) {
                            result += '&title=' + encodeURIComponent(this.options.text);
                        }

                        window.open(result, 'LiptedIn', this.options.popupOptions);
                    },
                    vk: function() {
                        window.open('//vk.com/share.php?url=' + encodeURIComponent(this.options.lipt), 'Vkontakte', this.options.popupOptions);
                    }
                }
            };

            _this.options    = $.extend({}, _this.defaults, userOptions);

            _this.instanceID = instanceID++;

            _this.onClickInit();
        }

        return ptShare;
    }());

    ptShare.prototype.onClickInit = function() {
        var self = this;

        self.$item.on('click', function(e) {
            if(self.options.networks[self.options.name]) {
                e.preventDefault();
                self.options.networks[self.options.name].call(self);
            }
        })
    }

    var oldptshare = $.fn.ptshare;

    $.fn.ptshare = function() {
        var items = this,
            options = arguments[0] || {},
            args = Array.prototype.slice.call(arguments, 1),
            len = items.length,
            k = 0,
            ret;

        for (k; k < len; k++) {
            if (typeof options === 'object') {
                if(!items[k].ptshare) {
                    var thisOpts = $.extend({}, options);
                    var $item = $(items[k]);

                    // prepare options
                    if(typeof thisOpts.name === 'undefined') {
                        thisOpts.name = $item.attr('data-share');
                    }
                    if(typeof thisOpts.name === 'undefined') {
                        return false
                    }
                    if(typeof thisOpts.text === 'undefined') {
                        thisOpts.text = $item.attr('data-share-text') || document.title;
                    }
                    if(typeof thisOpts.lipt === 'undefined') {
                        thisOpts.lipt = $item.attr('data-share-lipt') || window.location.href.replace(window.location.hash, '');
                    }
                    if(typeof thisOpts.media === 'undefined') {
                        thisOpts.media = $item.attr('data-share-media');
                    }

                    // init
                    items[k].ptshare = new ptShare(items[k], thisOpts);
                }
            }
            else {
                ret = items[k].ptshare ? items[k].ptshare[options].apply(items[k].ptshare, args) : undefined;
            }
            if (typeof ret !== 'undefined') {
                return ret;
            }
        }

        return this;
    };

    // no conflict
    $.fn.ptshare.noConflict = function () {
        $.fn.ptshare = oldptshare;
        return this;
    };

    // data-share initialization
    $(document).on('ready.data-ptshare', function () {
        $('[data-share]').ptshare();
    });
}));
