<?php

class Data_model extends CI_Model{
    
    private $accountsDB;
    private $sodDB;
    private $clanDB;
    private $itemlogDB;
    private $usersDB;

    function __construct(){
        parent::__construct();
        $this->accountsDB = $this->load->database('game_accounts', TRUE);
        $this->sodDB = $this->load->database('game_sod', TRUE);
        $this->clanDB = $this->load->database('game_clan', TRUE);
        $this->itemlogDB = $this->load->database('game_itemlog', TRUE);
        $this->testDB = $this->load->database('test', TRUE); 
    }

/**functions for website presentation*/    
    public function getArticles(){    
        return $this->db->where('visible',true)->order_by('date_start', 'DESC')->get('articles')->result();
    }

    public function getMixingFormulas(){
        return $this->db->get('mixing')->result();
    }

    public function getWeapons(){
        $categories = array('Axes','Bows','Claws','Daggers','Hammers','Javelins','Phantoms','Spears','Staffs','Swords');
        return $this->db->where('visible',true)->where_in('category', $categories)->order_by('level', 'ASC')->get('items')->result();
    }

    public function getDefenses(){
        $categories = array('Armors','Boots','Bracelets','Gauntlets','Orbs','Robes','Shields');
        return $this->db->where('visible',true)->where_in('category', $categories)->order_by('level', 'ASC')->get('items')->result();
    }

    public function getAccessories(){
        $categories = array('Amulets','Rings','Sheltoms');
        return $this->db->where('visible',true)->where_in('category', $categories)->order_by('level', 'ASC')->get('items')->result();
    }

    public function getBlessOwner(){
       /* datastruct is rsBLESS_CASTLE from onserver.h*/    
        $BLESS = GAMEPATH."BlessCastle.dat";
        $fp = fOpen($BLESS, "r");
        $data = fread($fp, filesize($BLESS)); 
        $CastleDetails = unpack("i/I", $data); 
        $owner=$CastleDetails[1];
        @fclose($fp);
        $ruler=$this->clanDB->query("select * from CL where MiconCnt = {$owner}")->result();
        $ruler=(object)$ruler[0];
        return (object) ['logo' => CLAN_IMAGE_URL.($ruler->MIconCnt).".bmp", 'name'=>$ruler->ClanName];
    }

    private function getPlayerInfo($name){
        
        if($res=$this->db->query("select * from characters where name='{$name}'")->result()){
            foreach($res as $r)
                return $r;
        }
        return (object)[
            "id"=>0,
            "name"=>$name,
            "class"=>"new",
            "level"=>"new",
            "modified"=>"new"
        ];
        
    }

    public function getOnlineList($showGM=LIST_SHOWGM){
        $list=[];
        $names= $showGM?"''":$this->resToQueryString($this->getGMCharacters(),'ign');

        $data=$this->clanDB->query("select distinct a.ChName from CT a left join (select * from CT)b on a.ChName=b.ChName where a.ChName not in (".$names.")")->result();

        foreach($data as $p){
            $list[]=$this->getPlayerInfo($p->ChName);
        }

        return $list;

    }

    public function updatePlayerRanking($mode=1){  
        return "This function has been removed.";
    }

    public function getPlayerRanking($cnt=0,$showGM=LIST_SHOWGM){
        $cnt=$cnt==0?"":"limit {$cnt}";
        $limit=date('Y-m-d',strtotime("-".RANKING_LASTACTIVE." days"));
        $gms=$showGM?"''":$this->resToQueryString($this->getGMCharacters(),'ign');
        return $this->db->query("SELECT * FROM revpt.characters  where modified >= '{$limit}' and level >=".RANKING_MINLVL." and deleted=0 and name not in ({$gms}) order by level desc, level_timestamp asc, exp desc {$cnt}")->result();
    }

    public function getTopInClassPlayers($showGM=LIST_SHOWGM){
        $top=[];
        $gms=$showGM?"''":$this->resToQueryString($this->getGMCharacters(),'ign');
        $limit=date('Y-m-d',strtotime("-".RANKING_LASTACTIVE." days"));
        for($i=1;$i<=10;$i++){
            $class=$this->getCharClass($i);
            $temp=$this->db->query("select * from characters  where class='{$class}' and deleted=0 and name not in ({$gms}) and modified >= '{$limit}' and level >=".RANKING_MINLVL." order by level desc limit 1")->result();
            $temp=$temp[0];
            $temp->title=$this->getCharTitle($i);
            $top[]=$temp;
        }

        usort($top,function($first,$second){
            return $first->level < $second->level;
        });

        return $top;
    }

    public function getSODSoloRanking(){
        $v=date('ym');
            if(count($this->sodDB->query("SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'sod2record{$v}'")->result())){
                $data = $this->sodDB->query("Select distinct a.CharName,a.CharType,a.Point,a.RegistDay  from sod2record{$v} as a join (Select CharName,max(Point) as Point from sod2record{$v} where point >0 group by CharName)as b on a.CharName=b.CharName where a.CharName=b.CharName and a.Point=b.Point order by Point desc")->result();
                $list=[];
                foreach($data as $rec){
                    $charType=$this->getCharClass($rec->CharType);
                    $regDate=new DateTime($rec->RegistDay);
                    $regDate=$regDate->format('M d @ g:i a');
                    $temp=(object) [
                        "name"=>$rec->CharName,
                        "type"=>$charType,
                        "score"=>number_format($rec->Point),
                        "date"=>$regDate
                    ];

                 $list[]=$temp;
                }

                return $list;
            }
        return $empty=[];
    }

    public function getClanList(){
        $clans=$this->clanDB->query("select CL.ClanName,CL.ClanZang,UL.ChType,CL.MIconCnt,CL.MemCnt,CL.Cpoint, NULL as SOD from CL left join UL on CL.ClanZang=UL.ChName order by Cpoint DESC")->result();
        $bcOwner=$this->getBlessOwner();

        $x=0;
        $topIndex=0;
        $topClan;
        $list=[];


        foreach($clans as $clan){
            if($x<3){
                $clan->SOD=$x+1;
            } 

            $charType=$this->getCharClass($clan->ChType);
            $temp=(object) [
                "name"=>$clan->ClanName,
                "logo"=>CLAN_IMAGE_URL.($clan->MIconCnt).".bmp",
                "score"=>number_format($clan->Cpoint),
                "chief"=>$clan->ClanZang,
                "chief_class"=>$charType,
                "members"=>$clan->MemCnt,
                "sod"=>$clan->SOD,
                "castle"=>false
            ];

            if($bcOwner->name==$clan->ClanName){
                $topIndex=$x;
                $temp->castle=true;
                $topClan=$temp;    
            }

            if($clan->MemCnt >= LIST_CLANMIN || $clan->SOD>0){    
                $list[]=$temp;
                $x++;
            }
        }

        if($topIndex!=0){
            unset($list[$topIndex]);
            array_unshift($list,$topClan);
        }
    
        return $list;
    }

    public function serverStatus(){
        $server['port'] = "80";
        $connection = @fsockopen(GAMEIP, GAMEPORT, $ERROR_NO, $ERROR_STR, (float)1.5);
        if($connection){
            fclose($connection);
            return "status-online";
        }else{
            return "status-offline";
        }
    }
    public function getSODClanList(){
        $list=[];
        
        $data=$this->clanDB->query("select CL.ClanName,CL.MIconCnt,CL.Cpoint,CL.ClanZang,UL.ChType from CL Left Join UL on CL.ClanZang=UL.ChName where Cpoint >0 order by Cpoint DESC")->result();
        
        foreach($data as $clan){
            $charType=$this->getCharClass($clan->ChType);

            $temp=(object) [
                "name"=>$clan->ClanName,
                "logo"=>CLAN_IMAGE_URL.($clan->MIconCnt).".bmp",
                "score"=>number_format($clan->Cpoint),
                "chief"=>$clan->ClanZang,
                "chief_class"=>$charType,
            ];
            
            $list[]=$temp;
        }
        return $list;     
    }
    
/** functions for account management */

    public function getUserDetails($username){
        $temp=addslashes(strtoupper($username));
        $prefix=$temp[0];
        $data=$this->accountsDB->query("select userid,Passwd from {$prefix}GameUser where userid='{$username}'")->result();
        if(count($data)){
            return $data;
        }
        return false;
    }

    public function getMemberDetails($email,$username){
        $temp=addslashes(strtoupper($username));
        $email=addslashes($email);
        $prefix=$temp[0];
        $data=$this->accountsDB->query("select Userid,Passwd,CUserName1,CUsername2,Email from {$prefix}PersonalMember where Email='{$email}'")->result();
        return count($data)?$data:false;
    }

    public function registerUser($data){
        $data->username=addslashes($data->username);
        $data->password=addslashes($data->password);
        $data->email=addslashes($data->email);
        $data->firstname=addslashes($data->firstname);
        $data->lastname=addslashes($data->lastname);
        

        $prefix=strtoupper($data->username);
        $prefix=$prefix[0];
        $t=time();

        $this->accountsDB->query("INSERT into {$prefix}GameUser(userid, Passwd, GPCode, RegistDay, DisuseDay, inuse, grade, EventChk, SelectChk, BlockChk, SpecialChk, Credit, DelChk)
                                 values ('{$data->username}','{$data->password}','PTP-RUD001', getdate(), '20300101',0, 0, 0, 0, 0, 0, 0, 0)");
        $this->accountsDB->query("INSERT into {$prefix}PersonalMember(PMNo,Userid,Passwd,CUserName1,CUsername2,Email) values ('{$t}','{$data->username}','{$data->password}','{$data->firstname}','{$data->lastname}','{$data->email}')");
        
        return true;
    }

    public function updatePassword($data){
        $data->username=addslashes($data->username);
        $data->password=addslashes($data->password);
        $prefix=strtoupper($data->username);
        $prefix=$prefix[0];

        if($this->accountsDB->query("UPDATE {$prefix}GameUser SET Passwd='{$data->password}' where userid='{$data->username}'")){
            return true;
        }
        return false;
    }

    public function getClansByUser($data){
        $data->username=addslashes($data->username);
        $data->password=addslashes($data->password);
        $prefix=strtoupper($data->username);
        $prefix=$prefix[0];

        $clanlist=[];

        if($clans=$this->clanDB->query("select ClanName,Note,ClanZang,RegiDate,MIconCnt from CL where UserID='{$data->username}' order by RegiDate DESC")->result()){
            foreach($clans as $clan){
                $members=$this->clanDB->query("select ChName,ChType,ChLv from UL where ClanName='{$clan->ClanName}' order by JoinDate DESC")->result();
                
                $mm=[];
                
                foreach($members as $mem){
                    $mm[]=(object) [
                        "name"=>$mem->ChName,
                        "type"=>$this->getCharClass($mem->ChType),
                        "level"=>$mem->ChLv
                    ];
                }

                $clan->members=$mm;

                $clanlist[]=$clan;
            }

            return $clanlist;
        }
        
        return false;    
    }


    public function updateClanNote($clan,$note){
        if($this->clanDB->query("UPDATE CL SET Note='{$note}' where MIconCnt='{$clan}'")){
            return true;
        }
        return false;
    }
    
    
    public function test(){  
        $this->accountsDB->query("update KGameUser set blockchk=0 where userid='kerlyn2'");
		return "done";
    }

    


/*** TEST FUNCTIONS */
    public function retrieveCastleData(){
        $BLESS = GAMEPATH."BlessCastle.dat";
        $fp = fOpen($BLESS, "r");
        $data = fread($fp, filesize($BLESS));
        $format="iCastleMode/"
                ."IdwMasterClan/"
                ."iDefenceLevel/"
                ."IdwLimitTime/"
                ."iCounter/"
                ."iTax/"
                ."iNextTax/"
                ."IdwBattleOverTime/"
                ."iSoulFountain_PotionCount/"
                ."IdwSoulFountainTime/"
                ."IdwSoulFountainTime2/"
                ."iSoulFountain_LastUserCount/"
                ."IdwStartTime/"
                ."s2TimeSec/"
                ."iSelectedSkill/"
                ."i10Temp/"
                ."iBlessCastleSetup_smTRANS_COMMAND_size/"
                ."iBlessCastleSetup_smTRANS_COMMAND_code/"
                ."iBlessCastleSetup_smTRANS_COMMAND_LParam/"
                ."iBlessCastleSetup_smTRANS_COMMAND_WParam/"
                ."iBlessCastleSetup_smTRANS_COMMAND_SParam/"
                ."iBlessCastleSetup_smTRANS_COMMAND_EParam/"
                ."iBlessCastleSetup_TaxRate/"
                ."s12BlessCastleSetup_Tower/"
                ."C4BlessCastleSetup_MercenaryNum/"
                ."iBlessCastleSetup_ClanSkill/"
                ."iBlessCastleSetup_Price/"
                ."IBlessCastleSetup_dwMasterClan/"
                ."i4BlessCastleSetup_Temp/"
                ."iClanTop10_size/"
                ."iClanTop10_code/"
                ."iClanTop10_tcode/"
                ."IClanTop10_dwObjectSerial/"
                ."IClanTop10_dwNameCode/"
                ."iClanTop10_x/"
                ."iClanTop10_y/"
                ."iClanTop10_z/"
                ."IClanTop10_dwCharCode/"
                ."IClanTop10_dwUpdateTime/"
                ."IClanTop10_dwTotalDamage/"
                ."c4ClanTop10_bCrystalTowerCount/"
                ."I2ClanTop10_dwTemp/"
                ."I10ClanTop10_dwUserCode/"
                ."I10ClanTop10_Damage/"
                ."I10ClanTop10_Counter/"
                ."xptr/"
                ."c4bCrystalSolderCount/"
                ."c4bCrystalTowerCount/"
                ."xptr2/"
                ."IdwScoreLogTime";

        $CastleDetails = unpack($format, $data); 
        @fclose($fp);
        
        $attackers=[];
        return $CastleDetails;
        for($i=1;$i<=10;$i++){
            if($CastleDetails["ClanTop10_Damage{$i}"]==0) continue;

            $temp["clanid"]=$CastleDetails["ClanTop10_dwUserCode{$i}"];
            $temp["damage"]=$CastleDetails["ClanTop10_Damage{$i}"];
            $attackers[]=$temp;
        }


        $clans=$this->resToQueryString($attackers,"clanid");
        $clans=$this->clanDB->query("select * from CL where MIconCnt in({$clans})")->result();
        
    
        $ret=[];
        foreach($clans as $clan){
            for($i=0;$i<count($attackers);$i++){
                if($clan->MIconCnt==$attackers[$i]['clanid']){
                    $ret[]=(object) ['logo' => CLAN_IMAGE_URL.($clan->MIconCnt).".bmp", 
                    'name'=>$clan->ClanName,
                    'score'=>number_format($attackers[$i]['damage'])];
                break;
                }
            }    
        }

        usort($ret,function($first,$second){
            return $first->score < $second->score;
        });
    
        return $ret;
    }

    public function clanInfo($name){
        $name=strtolower($name);
        $q=[];
        
        $q['clan_details']=$this->clanDB->query("select * from CL where lower(ClanName)  LIKE '{$name}'")->result();
        $q['clan_members']=$this->clanDB->query("select * from UL where lower(ClanName)  LIKE '{$name}'")->result();

        return $q;

    }

    public function getAllOnlineList(){
        $list=[];
        $data=$this->clanDB->query("select ChName,max(SNo),UserID,IP from CT group by ChName,UserID,IP")->result();
       
        foreach($data as $p){
            $temp['CT_Data']=$p;
            $temp['Aggregate_Data']=$this->getPlayerInfo($p->ChName);
            $list[]=$temp;
        }

        return $list;

    }

/****END:TEST FUNCTIONS */
 
/** function that pull from db-settings */

    private function getGMCharacters(){
        return $this->db->get('gm_characters')->result_array();
    }

    private function resToQueryString($arr,$colname){
        $string="";
        
            foreach($arr as $a){
                $val=$a["{$colname}"];
                $string.="'{$val}',";
            }
        
        return rtrim($string,",");
    }

    private function getCharClass($code=1){
        $class="fighter";
        switch($code){
            case 2: $class="mechanician"; break;
            case 3: $class="archer"; break;
            case 4: $class="pikeman"; break;
            case 5: $class="atalanta"; break;
            case 6: $class="knight"; break;
            case 7: $class="magician"; break;
            case 8: $class="priestess"; break;
            case 9: $class="assassin"; break;
            case 10: $class="shaman"; break;
        }
        return $class;
    }

    private function getCharTitle($code=1){
        $title=TITLE_TOP_FS;
        switch($code){
            case 2: $title=TITLE_TOP_MS; break;
            case 3: $title=TITLE_TOP_AS; break;
            case 4: $title=TITLE_TOP_PS; break;
            case 5: $title=TITLE_TOP_ATA; break;
            case 6: $title=TITLE_TOP_KS; break;
            case 7: $title=TITLE_TOP_MG; break;
            case 8: $title=TITLE_TOP_PRS; break;
            case 9: $title=TITLE_TOP_ASS; break;
            case 10: $title=TITLE_TOP_SHA; break;
        }
        return $title;
    }


/** functions for item pull from txt and zhoon files */

    private function read_file($File){
        // Finding the correct case file for unix webservers
        $File_Matches = $this->my_glob(GAMEPATH."GameServer/OpenItem/". $File);
        
        if(!count($File_Matches)) return false;
        
        // Reading the content of the file
        $Data = @file($File_Matches[0], FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);

        // If error, exit
        if(!$Data) return false;
        
        // Building a clean file
        $Clean = false;
        foreach($Data as $Line){
            if(strpos($Line, '//') === 0) continue; 
            $Line = trim(mb_convert_encoding($Line, "UTF-8", "EUC-KR"));
            if(empty($Line)) continue;
            $Clean .= $Line . PHP_EOL;
        }  
        return $Clean;
    }

    private function my_glob($Pattern){
        $Matches = array();
        
        $Folder = strtolower(dirname($Pattern));
        $Filename = basename($Pattern);
        
        if(is_dir($Folder)){
            if ($Handle = opendir($Folder)){
                while(false !== ($File = readdir($Handle))){
                    if ($File == "." || $File == "..") continue;
                    if(strcasecmp($Filename,$File) === 0) $Matches[] = $Folder . '/' . $File;
                }
                closedir($Handle);
            }
        }
        return $Matches;
    }

    private function read_item($Item_ID){
        
        $Item_File = $this->read_file($Item_ID . '.txt');
        if(!$Item_File) return false;
        
        preg_match('`^\*연결파일\s+"(.+)"`mi', $Item_File, $Item_Zhoon);
        if(!$Item_Zhoon)return false;
        
        $Zhoon_File = $this->read_file(trim(str_replace('\\', '/', $Item_Zhoon[1])));
        if(!$Zhoon_File) return false;
        
        $Item_Data = array();
        
        preg_match('`^\*E_NAME\s+"(.+)"`mi', $Zhoon_File, $Item_Name);
        if(!$Item_Name) return false;
        $Item_Data['Name'] = trim($Item_Name[1]);
        
        preg_match('`^\*코드\s+"(.+)"`mi', $Item_File, $Item_Code);
        if(!$Item_Code) return false;
        $Item_Data['Code'] = strtolower(trim($Item_Code[1]));
    
        preg_match('`^\*공격력\s+(\d+)\s+(\d+)\s+(\d+)\s+(\d+)`mi', $Item_File, $Item_Pow);
        if($Item_Pow) $Item_Data['Attack Power'] = array_slice($Item_Pow, 1, 4);
        
        preg_match('`^\*공격속도\s+(\d+)`mi', $Item_File, $Item_Velocidade);
        if($Item_Velocidade) $Item_Data['Weap. Speed'] = $Item_Velocidade[1];
        
        preg_match('`^\*사정거리\s+(\d+)`mi', $Item_File, $Item_Range);
        if($Item_Range) $Item_Data['Range'] = $Item_Range[1];
    
        preg_match('`^\*크리티컬\s+(\d+)`mi', $Item_File, $Item_Critical);
        if($Item_Critical) $Item_Data['Critical'] = $Item_Critical[1];
    
        preg_match('`^\*방어력\s+(\d+)(?:\s+(\d+))?`mi', $Item_File, $Item_Defence);
        if($Item_Defence) $Item_Data['Defence'] = array_slice($Item_Defence, 1, 2);
    
        preg_match('`^\*명중력\s+(\d+)(?:\s+(\d+))?`mi', $Item_File, $Item_Rating);
        if($Item_Rating) $Item_Data['Attack Rating'] = array_slice($Item_Rating, 1, 2);
    
        preg_match('`^\*흡수력\s+([0-9.]+)(?:\s+([0-9.]+))?`mi', $Item_File, $Item_Absorb);
        if($Item_Absorb) $Item_Data['Absorb'] = array_slice($Item_Absorb, 1, 2);
    
        preg_match('`^\*블럭율\s+(\d+)(?:\s+(\d+))?`mi', $Item_File, $Item_Block);
        if($Item_Block) $Item_Data['Block'] = array_slice($Item_Block, 1, 2);
    
        preg_match('`^\*이동속도\s+([0-9.]+)(?:\s+([0-9.]+))?`mi', $Item_File, $Item_Run_Speed);
        if($Item_Run_Speed) $Item_Data['Run Speed'] = array_slice($Item_Run_Speed, 1, 2);
    
        preg_match('`^\*생명력재생\s+([0-9.]+)(?:\s+([0-9.]+))?`mi', $Item_File, $Item_HP_Regen);
        if($Item_HP_Regen) $Item_Data['HP Regen'] = array_slice($Item_HP_Regen, 1, 2);
    
        preg_match('`^\*기력재생\s+([0-9.]+)(?:\s+([0-9.]+))?`mi', $Item_File, $Item_MP_Regen);
        if($Item_MP_Regen) $Item_Data['MP Regen'] = array_slice($Item_MP_Regen, 1, 2);
    
        preg_match('`^\*근력재생\s+([0-9.]+)(?:\s+([0-9.]+))?`mi', $Item_File, $Item_STM_Regen);
        if($Item_STM_Regen) $Item_Data['STM Regen'] = array_slice($Item_STM_Regen, 1, 2);
    
        preg_match('`^\*생명력추가\s+(\d+)(?:\s+(\d+))?`mi', $Item_File, $Item_HP);
        if($Item_HP) $Item_Data['Add HP'] = array_slice($Item_HP, 1, 2);
    
        preg_match('`^\*기력추가\s+(\d+)(?:\s+(\d+))?`mi', $Item_File, $Item_MP);
        if($Item_MP) $Item_Data['Add MP'] = array_slice($Item_MP, 1, 2);
    
        preg_match('`^\*근력추가\s+(\d+)(?:\s+(\d+))?`mi', $Item_File, $Item_STM);
        if($Item_STM) $Item_Data['Add STM'] = array_slice($Item_STM, 1, 2);
    
        preg_match('`^\*보유공간\s+(\d+)`mi', $Item_File, $Item_Potion_Count);
        if($Item_Potion_Count) $Item_Data['Potion Count'] = $Item_Potion_Count[1];
    
        preg_match('`^\*생체\s+(\d+)(?:\s+(\d+))?`mi', $Item_File, $Item_Earth);
        if($Item_Earth) $Item_Data['Organic'] = array_slice($Item_Earth, 1, 2);
    
        preg_match('`^\*불\s+(\d+)(?:\s+(\d+))?`mi', $Item_File, $Item_Flame);
        if($Item_Flame) $Item_Data['Fire'] = array_slice($Item_Flame, 1, 2);
    
        preg_match('`^\*냉기\s+(\d+)(?:\s+(\d+))?`mi', $Item_File, $Item_Frost);
        if($Item_Frost) $Item_Data['Frost'] = array_slice($Item_Frost, 1, 2);
    
        preg_match('`^\*번개\s+(\d+)(?:\s+(\d+))?`mi', $Item_File, $Item_Light);
        if($Item_Light) $Item_Data['Ligntning'] = array_slice($Item_Light, 1, 2);
    
        preg_match('`^\*독\s+(\d+)(?:\s+(\d+))?`mi', $Item_File, $Item_Poison);
        if($Item_Poison) $Item_Data['Poison'] = array_slice($Item_Poison, 1, 2);
    
        preg_match('`^\*생명력상승\s+(\d+)(?:\s+(\d+))?`mi', $Item_File, $Item_Restore_HP);
        if($Item_Restore_HP) $Item_Data['Restauracao de HP'] = array_slice($Item_Restore_HP, 1, 2);
    
        preg_match('`^\*기력상승\s+(\d+)(?:\s+(\d+))?`mi', $Item_File, $Item_Restore_MP);
        if($Item_Restore_MP) $Item_Data['Restauracao de MP'] = array_slice($Item_Restore_MP, 1, 2);
    
        preg_match('`^\*근력상승\s+(\d+)(?:\s+(\d+))?`mi', $Item_File, $Item_Restore_STM);
        if($Item_Restore_STM) $Item_Data['Restauracao de STM'] = array_slice($Item_Restore_STM, 1, 2);
    
        preg_match('`^\*레벨\s+(\d+)`mi', $Item_File, $Item_Level);
        if($Item_Level) $Item_Data['Req. Level'] = $Item_Level[1];
    
        preg_match('`^\*힘\s+(\d+)`mi', $Item_File, $Item_Strength);
        if($Item_Strength) $Item_Data['Req. Strength'] = $Item_Strength[1];
    
        preg_match('`^\*정신력\s+(\d+)`mi', $Item_File, $Item_Spirit);
        if($Item_Spirit) $Item_Data['Req. Spirit'] = $Item_Spirit[1];
    
        preg_match('`^\*재능\s+(\d+)`mi', $Item_File, $Item_Talent);
        if($Item_Talent) $Item_Data['Req. Talent'] = $Item_Talent[1];
    
        preg_match('`^\*민첩성\s+(\d+)`mi', $Item_File, $Item_Agility);
        if($Item_Agility) $Item_Data['Req. Agility'] = $Item_Agility[1];
        
        preg_match('`^\*\*방어력\s+(\d+)(?:\s+(\d+))?`mi', $Item_File, $Item_Spec_Defence);
        if($Item_Spec_Defence) $Item_Data['Add. Defence'] = array_slice($Item_Spec_Defence, 1, 2);
    
        preg_match('`^\*\*흡수력\s+([0-9.]+)(?:\s+([0-9.]+))?`mi', $Item_File, $Item_Spec_Absorb);
        if($Item_Spec_Absorb) $Item_Data['Add. Absorb'] = array_slice($Item_Spec_Absorb, 1, 2);
    
        preg_match('`^\*\*블럭율\s+(\d+)(?:\s+(\d+))?`mi', $Item_File, $Item_Spec_Block);
        if($Item_Spec_Block) $Item_Data['Add. Block'] = array_slice($Item_Spec_Block, 1, 2);
    
        preg_match('`^\*\*이동속도\s+([0-9.]+)(?:\s+([0-9.]+))?`mi', $Item_File, $Item_Spec_Run_Speed);
        if($Item_Spec_Run_Speed) $Item_Data['Add. Run Speed'] = array_slice($Item_Spec_Run_Speed, 1, 2);
    
        preg_match('`^\*\*마법숙련도\s+(\d+)`mi', $Item_File, $Item_Magic_APT);
        if($Item_Magic_APT) $Item_Data['Magic APT'] = $Item_Magic_APT[1];
    
        //preg_match('`^\*\*생명력재생\s+([0-9.]+)(?:\s+([0-9.]+))?`mi', $Item_File, $Item_Spec_HP_Regen);
        //if($Item_Spec_HP_Regen) $Item_Data['Spec. HP Regen'] = array_slice($Item_Spec_HP_Regen, 1, 2);
    
        preg_match('`^\*\*생명력재생\s+([0-9.]+)`mi', $Item_File, $Item_Spec_HP_Regen);
        if($Item_Spec_HP_Regen) $Item_Data['Spec HP Regen'] = $Item_Spec_HP_Regen[1];
    
        preg_match('`^\*\*기력재생\s+([0-9.]+)(?:\s+([0-9.]+))?`mi', $Item_File, $Item_Spec_MP_Regen);
        if($Item_Spec_MP_Regen) $Item_Data['Spec MP Regen'] = array_slice($Item_Spec_MP_Regen, 1, 2);
    
        preg_match('`^\*\*근력재생\s+([0-9.]+)(?:\s+([0-9.]+))?`mi', $Item_File, $Item_Spec_STM_Regen);
        if($Item_Spec_STM_Regen) $Item_Data['Spec STM Regen'] = array_slice($Item_Spec_STM_Regen, 1, 2);
    
        preg_match('`^\*\*공격력\s+0\s+(\d+)`mi', $Item_File, $Item_Spec_Pow);
        if($Item_Spec_Pow) $Item_Data['Add. Attack Power'] = $Item_Spec_Pow[1];
    
        preg_match('`^\*\*명중력\s+(\d+)\s+(\d+)`mi', $Item_File, $Item_Spec_Rating);
        if($Item_Spec_Rating) $Item_Data['Attack Rating Level/'] = array_splice($Item_Spec_Rating, 1, 2);
    
        preg_match('`^\*\*크리티컬\s+(\d+)`mi', $Item_File, $Item_Spec_Critical);
        if($Item_Spec_Critical) $Item_Data['Add. Critical'] = $Item_Spec_Critical[1];
    
        preg_match('`^\*\*공격속도\s+(\d+)`mi', $Item_File, $Item_Spec_Speed);
        if($Item_Spec_Speed) $Item_Data['Add. Weap. Speed'] = $Item_Spec_Speed[1];
    
        preg_match('`^\*\*사정거리\s+(\d+)`mi', $Item_File, $Item_Spec_Range);
        if($Item_Spec_Range) $Item_Data['Add. Range'] = $Item_Spec_Range[1];
        
        return $Item_Data;
    }

    
    private function retrieveItems($Item_Type, $Start, $End){
        $Items=[];

        if($Start > $End){
            $Temp = $Start;
            $Start = $End;
            $End = $Start;
        }
        
        $Prefetch = array();
        $Sorting = array();
        $Sorting2 = array();
        
        for($i = $Start, $j = 0; $i <= $End; $i++){
            $Item_Data = $this->read_item($Item_Type . $i);
            if($Item_Data){
                $Prefetch[$j] = $Item_Data;
                $Level = (isset($Item_Data['Req. Level'])) ? $Item_Data['Req. Level'] : 0;
                $Sorting[] = array('Level' => $Level, 'Key' => $j);
                $j++;
            }
        }
        
        $Level = array();
        $Key = array();
        
        foreach($Sorting as $Index => $Row){
            $Level[$Index] = $Row['Level'];
            $Key[$Index] = $Row['Key'];
        }
        
        array_multisort($Level, SORT_ASC, $Key, SORT_ASC, $Sorting);        
        foreach($Sorting as $Index => $Row){
            $Items[] = $Prefetch[$Row['Key']];
        }
        return($Items);
    }

    public function initializeItemList(){

        $this->db->truncate('items');
        $items=array_merge($this->itemListFormatter("armors",$this->retrieveItems('DA',102,131)),$this->itemListFormatter("armors",$this->retrieveItems('DA',301,302)));
        $items=array_merge($items,array_merge($this->itemListFormatter("robes",$this->retrieveItems('DA',202,231)),$this->itemListFormatter("robes",$this->retrieveItems('DA',401,402))));
        $items=array_merge($items,$this->itemListFormatter("amulets",$this->retrieveItems('OA', 101, 153)));
        $items=array_merge($items,$this->itemListFormatter("axes",$this->retrieveItems('WA', 101, 303)));
        $items=array_merge($items,$this->itemListFormatter("boots",$this->retrieveItems('DB', 101, 150)));
        $items=array_merge($items,$this->itemListFormatter("bows",$this->retrieveItems('WS', 101, 131)));
        $items=array_merge($items,$this->itemListFormatter("bracelets",$this->retrieveItems('OA', 201, 228)));
        $items=array_merge($items,$this->itemListFormatter("claws",$this->retrieveItems('WC', 101, 303)));
        $items=array_merge($items,$this->itemListFormatter("daggers",$this->retrieveItems('WD', 101, 303)));
        $items=array_merge($items,$this->itemListFormatter("gauntlets",$this->retrieveItems('DG', 101, 150)));
        $items=array_merge($items,$this->itemListFormatter("hammers",$this->retrieveItems('WH', 101, 303)));
        $items=array_merge($items,$this->itemListFormatter("javelins",$this->retrieveItems('WT', 101, 303)));
        $items=array_merge($items,$this->itemListFormatter("orbs",$this->retrieveItems('OM', 101, 303)));
        $items=array_merge($items,$this->itemListFormatter("phantoms",$this->retrieveItems('WN', 101, 303)));
        $items=array_merge($items,$this->itemListFormatter("rings",$this->retrieveItems('OR', 101, 253)));
        $items=array_merge($items,$this->itemListFormatter("spears",$this->retrieveItems('WP', 101, 304)));
        $items=array_merge($items,$this->itemListFormatter("sheltoms",$this->retrieveItems('OS', 101, 200)));
        $items=array_merge($items,$this->itemListFormatter("shields",$this->retrieveItems('DS', 101, 304)));
        $items=array_merge($items,$this->itemListFormatter("staffs",$this->retrieveItems('WM', 101, 303)));
        $items=array_merge($items,$this->itemListFormatter("swords",$this->retrieveItems('WS', 201, 233)));
        $this->db->insert_batch('items', $items);
        
        $time = microtime(true) - $_SERVER["REQUEST_TIME_FLOAT"];
        return "Item DB set in : {$time} seconds";
         
    }

    private function rangeFormatter($label,$item,$attribute){
        $hLimit=isset($item["{$attribute}"][1])?" - {$item["{$attribute}"][1]}":"";
        return "<li>{$label} &nbsp;<span>{$item["{$attribute}"][0]}{$hLimit}</span></li>";
    }

    private function itemListFormatter($category='uncategorized',$items){
        $formattedData=[];
        
        foreach($items as $item){
            $temp=[];
            $temp['category']=$category;
            $temp['name']=$item['Name'];
            $temp['code']=$item['Code'];
            

                $attributes="";
                $requirements="";
                $spec="";

                    if(isset($item['Attack Power'])){
                        $attributes.="<li>Attack Power &nbsp;<span>{$item['Attack Power'][0]}({$item['Attack Power'][1]}) - {$item['Attack Power'][2]}({$item['Attack Power'][3]})</span></li>";
                    }
                    if(isset($item['Weap. Speed'])){
                        $attributes.="<li>Weapon Speed &nbsp;<span>{$item['Weap. Speed']}</span></li>";
                    }
                    if(isset($item['Range'])){
                        $attributes.="<li>Range &nbsp;<span>{$item['Range']}</span></li>";
                    }
                    if(isset($item['Critical'])){
                        $attributes.="<li>Critical &nbsp;<span>{$item['Critical']}</span></li>";
                    }
                    if(isset($item['Defence'])){
                        $attributes.=$this->rangeFormatter("Defense",$item,"Defence");
                    }
                    if(isset($item['Attack Rating'])){
                        $attributes.=$this->rangeFormatter("Attack Rating",$item,"Attack Rating");
                    }
                    if(isset($item['Absorb'])){
                        $attributes.=$this->rangeFormatter("Absorb",$item,"Absorb");
                    }
                    if(isset($item['Block'])){
                        $attributes.=$this->rangeFormatter("Block",$item,"Block");
                    }
                    if(isset($item['Run Speed'])){
                        $attributes.=$this->rangeFormatter("Running Speed",$item,"Run Speed");
                    }
                    if(isset($item['HP Regen'])){
                        $attributes.=$this->rangeFormatter("HP Regen",$item,"HP Regen");
                    }
                    if(isset($item['MP Regen'])){
                        $attributes.=$this->rangeFormatter("MP Regen",$item,"MP Regen");
                    }
                    if(isset($item['STM Regen'])){
                        $attributes.=$this->rangeFormatter("STM Regen",$item,"STM Regen");
                    }
                    if(isset($item['Add HP'])){
                        $attributes.=$this->rangeFormatter("Add HP",$item,"Add HP");
                    }
                    if(isset($item['Add MP'])){
                        $attributes.=$this->rangeFormatter("Add MP",$item,"Add MP");
                    }
                    if(isset($item['Add STM'])){
                        $attributes.=$this->rangeFormatter("Add STM",$item,"Add STM");
                    }
                    if(isset($item['Potion Count'])){
                        $attributes.="<li>Potion Capacity &nbsp;<span>{$item['Potion Count']}</span></li>";
                    }
                    if(isset($item['Organic'])){
                        $attributes.=$this->rangeFormatter("Organic Resistance",$item,"Organic");
                    }
                    if(isset($item['Fire'])){
                        $attributes.=$this->rangeFormatter("Fire Resistance",$item,"Fire");
                    }
                    if(isset($item['Frost'])){
                        $attributes.=$this->rangeFormatter("Frost Resistance",$item,"Frost");
                    }
                    if(isset($item['Ligntning'])){
                        $attributes.=$this->rangeFormatter("Lightning Resistance",$item,"Ligntning");
                    }
                    if(isset($item['Poison'])){
                        $attributes.=$this->rangeFormatter("Poison Resistance",$item,"Poison");
                    }
                    if(isset($item['Restauracao de HP'])){
                        $attributes.=$this->rangeFormatter("HP Restoration",$item,"Restauracao de HP");
                    }
                    if(isset($item['Restauracao de MP'])){
                        $attributes.=$this->rangeFormatter("MP Restoration",$item,"Restauracao de MP");
                    }
                    if(isset($item['Restauracao de STM'])){
                        $attributes.=$this->rangeFormatter("STM Restoration",$item,"Restauracao de STM");
                    }
                    if(isset($item['Req. Level'])){
                        $requirements.="<li>Level &nbsp;<span>{$item['Req. Level']}</span></li>";
                    }
                    if(isset($item['Req. Spirit'])){
                        $requirements.="<li>Spirit &nbsp;<span>{$item['Req. Spirit']}</span></li>";
                    }
                    if(isset($item['Req. Strength'])){
                        $requirements.="<li>Strength &nbsp;<span>{$item['Req. Strength']}</span></li>";
                    }
                    if(isset($item['Req. Talent'])){
                        $requirements.="<li>Talent &nbsp;<span>{$item['Req. Talent']}</span></li>";
                    }
                    if(isset($item['Req. Agility'])){
                        $requirements.="<li>Agility &nbsp;<span>{$item['Req. Agility']}</span></li>";
                    }
                    if(isset($item['Add. Defence'])){
                        $spec.=$this->rangeFormatter("+Defense",$item,"Add. Defence");
                    }
                    if(isset($item['Add. Absorb'])){
                        $spec.=$this->rangeFormatter("+Absorb",$item,"Add. Absorb");
                    }
                    if(isset($item['Add. Block'])){
                        $spec.=$this->rangeFormatter("+Block",$item,"Add. Block");
                    }
                    if(isset($item['Add. Run Speed'])){
                        $spec.=$this->rangeFormatter("+Running Speed",$item,"Add. Run Speed");
                    }
                    if(isset($item['Magic APT'])){
                        $spec.="<li>+Magic Aptitude &nbsp;<span>{$item['Magic APT']}</span></li>";
                    }
                    if(isset($item['Spec HP Regen'])){
                        $spec.="<li>+HP Regen &nbsp;<span>{$item['Spec HP Regen']}</span></li>";
                    }
                    if(isset($item['Spec MP Regen'])){
                        $spec.=$this->rangeFormatter("+MP Regen",$item,"Spec MP Regen");
                    }
                    if(isset($item['Spec STM Regen'])){
                        $spec.=$this->rangeFormatter("+MP Regen",$item,"Spec STM Regen");
                    }
                    if(isset($item['Attack Rating Level/'])){
                        $spec.=$this->rangeFormatter("+Attack Rating Lvl/x",$item,"Attack Rating Level/");
                    }
                    if(isset($item['Add. Attack Power'])){
                        $spec.="<li>+Attack Power Lvl/x&nbsp;<span>{$item['Add. Attack Power']}</span></li>";
                    }
                    if(isset($item['Add. Critical'])){
                        $spec.="<li>+Critical &nbsp;<span>{$item['Add. Critical']}</span></li>";
                    }
                    if(isset($item['Add. Weap. Speed'])){
                        $spec.="<li>+Weapon Speed &nbsp;<span>{$item['Add. Weap. Speed']}</span></li>";
                    }
                    if(isset($item['Add. Range'])){
                        $spec.="<li>+Range &nbsp;<span>{$item['Add. Range']}</span></li>";
                    }
                
                $temp['attributes']=$attributes;
                $temp['requirements']=$requirements;
                $temp['spec']=$spec;
           
            $formattedData[]=$temp;
        }//end for-each
      return $formattedData;
    }

}