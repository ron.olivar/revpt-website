<?php
class Pages extends CI_Controller {

    function __construct(){
        parent::__Construct();
        $this->load->helper('url');
        $this->load->library('recaptcha');
        $this->load->model('data_model');
        date_default_timezone_set("Asia/Kuala_Lumpur");
    }

    public function view($page = 'home'){
        if ( ! file_exists(APPPATH.'views/pages/'.$page.'.php')){
                // Whoops, we don't have a page for that!
                show_404();
                exit;
        } 
        
        $data['mainSlider']=false;
        $data['bless']=$this->data_model->getBlessOwner();
        $data['server_status']=$this->data_model->serverStatus();
        $data['online_players']=$this->data_model->getOnlineList();
        $data['sod_clans']=$this->data_model->getSODClanList();

        
        if($page=='home'){
            $data['mainSlider']=true;
            $data['articles']=$this->data_model->getArticles();
            $data['toplist']=$this->data_model->getPlayerRanking(10);
            $data['topclass']=$this->data_model->getTopInClassPlayers();
        }elseif($page=='upgrade-mixing'){
            $data['formulas']=$this->data_model->getMixingFormulas();
        }elseif($page=='items-weapons'){
            $data['items']=$this->data_model->getWeapons();
        }elseif($page=='items-defenses'){
            $data['items']=$this->data_model->getDefenses();
        }elseif($page=='items-accessories'){
            $data['items']=$this->data_model->getAccessories();
        }elseif($page=='ranking-clan'){
            $data['clans']=$this->data_model->getClanList();
        }elseif($page=='ranking-bellatra-solo'){
            $data['list']=$this->data_model->getSODSoloRanking();
        }elseif($page=='ranking-level'){
            $data['list']=$this->data_model->getPlayerRanking();
        }
        
        

        $this->load->view('templates/header', $data);
        $this->load->view('templates/navbar');
        $this->load->view('pages/'.$page,$data);
        $this->load->view('templates/footer');            
    }
}