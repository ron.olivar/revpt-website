<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Forms extends CI_Controller {
    function __construct(){
        parent::__Construct();
        $this->load->helper('url');
        $this->load->library(array('recaptcha','form_validation'));
        $this->load->model('data_model');
    }

    public function register(){
       
        
        $this->form_validation->set_rules('username', 'Username', array('trim','required','min_length[5]','max_length[25]','alpha_numeric',array(
            'begins_with_letter',
            function($str){ 
                return ctype_alpha($str[0]);
            }
        )));
        $this->form_validation->set_message('begins_with_letter', '{field} must begin with a letter.');
        
        $this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[6]|max_length[25]|alpha_numeric');
        $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
        $this->form_validation->set_rules('firstname', 'Firstname', 'trim|required|min_length[3]|max_length[15]|alpha_numeric');
        $this->form_validation->set_rules('lastname', 'Lastname', 'trim|required|min_length[3]|max_length[15]|alpha_numeric');
        $recaptcha = $this->input->post('g-recaptcha-response');


        
        $errors="";

        if ($this->form_validation->run() == FALSE){
            $errors.=validation_errors();  
        }

        if(!form_error('username') && $this->data_model->getUserDetails($this->input->post('username'))){
            $errors.="<p>Username is already taken.</p>";
        }

        if(!form_error('username') && !form_error('email') && $this->data_model->getMemberDetails($this->input->post('email'),$this->input->post('username'))){
            $errors.="<p>Email is already taken.</p>";
        }

        
        if (!empty($recaptcha)){
            $response = $this->recaptcha->verifyResponse($recaptcha);
            if (isset($response['success']) and $response['success'] === true) {
                //echo "You got it!";
            }else{
                $errors.="<p>Please verify that you are not a Robot.</p>";
            }
        }else{
            $errors.="<p>Please verify that you are not a Robot.</p>"; 
        }

        if(strlen($errors)){
            $errors="<div class='pt-info-box text-danger'><div class='pt-info-box-icon'><i class='ion-close-round'></i></div><h3>Error!</h3><em>{$errors}</em></div>";
            echo json_encode(array('response'=>$errors));
        }else{
            $data=(object)[
                "username"=>$this->input->post('username'),
                "password"=>$this->input->post('password'),
                "email"=>$this->input->post('email'),
                "firstname"=>$this->input->post('firstname'),
                "lastname"=>$this->input->post('lastname')
            ];
            if($this->data_model->registerUser($data)){
                $success="Welcome to RevolutionPT ".ucfirst(strtolower($this->input->post('firstname')))."! Your account has been created. <br/><small>Please reload the page to register another account.</small>";
                $success="<div class='pt-info-box text-success'><div class='pt-info-box-icon'><i class='ion-checkmark-round'></i></div><h3>Success!</h3><em>{$success}</em></div>";
                echo json_encode(array('type'=>'success','response'=>$success));  
            }else{
                $errors="<div class='pt-info-box text-danger'><div class='pt-info-box-icon'><i class='ion-close-round'></i></div><h3>Error!</h3><em>There was a problem creating your account. Please retry after a few minutes.</em></div>";
                echo json_encode(array('response'=>$errors));
            }
        }
    }

    public function changePW(){
        $this->load->library(array('recaptcha','form_validation'));
        $this->form_validation->set_rules('username', 'Username', 'trim|required|alpha_numeric');
        $this->form_validation->set_rules('oldpassword', 'Old Password', 'trim|required');
        $this->form_validation->set_rules('newpassword', 'New Password', 'trim|required|min_length[6]|max_length[12]|alpha_numeric|differs[oldpassword]');
        $recaptcha = $this->input->post('g-recaptcha-response');
        
        $errors="";

        if ($this->form_validation->run() == FALSE){
            $errors.=validation_errors();  
        }else{
            $username=$this->input->post('username');
            if(ctype_alpha($username[0]) && $user=$this->data_model->getUserDetails($username)){
                if($this->input->post('oldpassword')!=$user[0]->Passwd){
                    $errors.="<p>Old password is invalid.";
                }
            }else{
                $errors.="<p>Account not found.</p>";
            }
        }
        
        if (!empty($recaptcha)){
            $response = $this->recaptcha->verifyResponse($recaptcha);
            if (isset($response['success']) and $response['success'] === true) {
                //echo "You got it!";
            }else{
                $errors.="<p>Please verify that you are not a Robot.</p>";
            }
        }else{
            $errors.="<p>Please verify that you are not a Robot.</p>"; 
        }

       
        if(strlen($errors)){
            $errors="<div class='pt-info-box text-danger'><div class='pt-info-box-icon'><i class='ion-close-round'></i></div><h3>Error!</h3><em>{$errors}</em></div>";
            echo json_encode(array('response'=>$errors));
        }else{
            $data=(object)[
                "username"=>$this->input->post('username'),
                "password"=>$this->input->post('newpassword')
            ];

            if($this->data_model->updatePassword($data)){
                $success=$this->input->post('username').", your password has been updated.";
                $success="<div class='pt-info-box text-success'><div class='pt-info-box-icon'><i class='ion-checkmark-round'></i></div><h3>Success!</h3><em>{$success}</em></div>";
                echo json_encode(array('type'=>'success','response'=>$success));  
            }else{
                $errors="<div class='pt-info-box text-danger'><div class='pt-info-box-icon'><i class='ion-close-round'></i></div><h3>Error!</h3><em>There was a problem updating your account. Please retry after a few minutes.</em></div>";
                echo json_encode(array('response'=>$errors));
            }
 
        }
    }

    public function updateClan(){
        $errors="";
        $data=[];

        if(!$this->input->post('clanid')){
            return json_encode(array("response"=>"There was a problem updating your clan. Please retry in a few minutes."));
        }
        
        if($this->input->post('notice')){
            $this->form_validation->set_rules('notice', 'Clan Note', array('trim','max_length[80]',array('allowed_chars',function($str){
                if(preg_match('/^[a-zA-Z0-9,.!? ]*$/i', $str)){
                    return $str;
                }
                return false;
            })));
            $this->form_validation->set_message('allowed_chars', '{field} can only contain letters, numbers, dots, punctuation, and spaces.');

            if ($this->form_validation->run() == FALSE){
              $errors.=validation_errors(); 
            }
            
            if($this->form_validation->run() && $this->data_model->updateClanNote($this->input->post('clanid'),$this->input->post('notice'))){
              $data['note']=$this->input->post('notice');
            }
        }

        if((!$this->input->post('notice') || ($this->input->post('notice') && $this->form_validation->run())) && !empty($_FILES['logo']['name'])){
            $config['upload_path']="./assets/images/clanimage/";
            $config['allowed_types']='bmp|BMP|jpg|JPG|gif|GIF|png|PNG|tmp';
            $config['max_size']= 512;
            $config['overwrite']=true;
            $config['file_name']=$this->input->post('clanid').".tmp";
    
            $this->load->library('upload',$config);
            
            if($this->upload->do_upload("logo")){
                $uploaded = $this->upload->data();
                
                try{
                    //convert to bmp
                    switch($uploaded['file_type']){
                        case 'image/jpeg':$im = imagecreatefromjpeg($uploaded['full_path']); break;
                        case 'image/png':$im = imagecreatefrompng($uploaded['full_path']);break;
                        case 'image/gif':$im = imagecreatefromgif($uploaded['full_path']);break;
                        default: $im = imagecreatefrombmp($uploaded['full_path']); 
                    }
                
                    $im=imagescale($im, 32, 32);
                    imagebmp($im,CLAN_IMAGE_ABSOLUTE.$uploaded['raw_name'].".bmp");
                    imagedestroy($im);

                    
                    
                    $data['image']=CLAN_IMAGE_URL.$uploaded['raw_name'].".bmp?t=".time();
                   
                    
                    //delete temporary file
                    if(file_exists($uploaded['full_path'])){
                        unlink($uploaded['full_path']);
                    }


                }catch(Exception $e){
                    $errors.='<p>failed to convert image</p>';
                }
                

            }else{
                $errors.=$this->upload->display_errors();
            }

        }
        if(strlen($errors)){
            $errors="<div class='pt-info-box text-danger'><div class='pt-info-box-icon'><i class='ion-close-round'></i></div><h3>Error.</h3><em>{$errors}</em></div>";
            echo json_encode(array('type'=>'error','response'=>$errors));  
        }else{

            $success='Clan information has been updated';
            $success="<div class='pt-info-box text-success'><div class='pt-info-box-icon'><i class='ion-checkmark-round'></i></div><h3>Success!</h3><em>{$success}</em></div>";
            echo json_encode(array('type'=>'success','response'=>$success,'data'=>$data));  
        }
        
    }

    public function manageClan(){
        $this->load->library(array('recaptcha','form_validation'));        
        $this->form_validation->set_rules('username', 'Username', 'trim|required');
        $this->form_validation->set_rules('password', 'Password', 'trim|required');
        $recaptcha = $this->input->post('g-recaptcha-response');
        
        $errors="";

        if ($this->form_validation->run() == FALSE){
            $errors.=validation_errors();  
        }else{
            $username=$this->input->post('username');
            if(ctype_alpha($username[0]) && $user=$this->data_model->getUserDetails($username)){
                if($this->input->post('password')!=$user[0]->Passwd){
                    $errors.="<p>password is invalid.</p>";
                }
            }else{
                $errors.="<p>Account not found.</p>";
            }
        }
        
        if (!empty($recaptcha)){
            $response = $this->recaptcha->verifyResponse($recaptcha);
            if (isset($response['success']) and $response['success'] === true) {
                //echo "You got it!";
            }else{
                $errors.="<p>Please verify that you are not a Robot.</p>";
            }
        }else{
            $errors.="<p>Please verify that you are not a Robot.</p>"; 
        }

        if(strlen($errors)){
            $errors="<div class='pt-info-box text-danger'><div class='pt-info-box-icon'><i class='ion-close-round'></i></div><h3>Error!</h3><em>{$errors}</em></div>";
            echo json_encode(array('response'=>$errors)); 
        }else{
            $data=(object)[
                "username"=>$this->input->post('username'),
                "password"=>$this->input->post('newpassword')
            ];
            if($clans=$this->data_model->getClansByUser($data)){

                $forms="";
                foreach($clans as $clan){
                    $forms.=$this->load->view('templates/clan_management_panel',$clan,true);
                }
                
                echo json_encode(array('type'=>'success','response'=>$forms)); 
            }else{
               $errors="Only Clan Chiefs may access this panel.";
               $errors="<div class='pt-info-box text-danger'><div class='pt-info-box-icon'><i class='ion-close-round'></i></div><h3>Error!</h3><em>{$errors}</em></div>";
               echo json_encode(array('response'=>$errors)); 
            }
        }
           
    }
	
}
