<?php
class Scraper extends CI_Controller {

    function __construct(){
        parent::__Construct();
        $this->load->helper('url');
        $this->load->library('recaptcha');
        $this->load->model('data_model');
        date_default_timezone_set("Asia/Kuala_Lumpur");
    }

    public function scrape_items(){
        echo "<pre>";
            print_r($this->data_model->initializeItemList());
        echo "</pre>";
    }

    public function test(){
        $time_start = microtime(true); 
        echo "<pre>";
            print_r($this->data_model->test());
        echo "</pre>";
        $time_end = microtime(true);
        $time = $time_end - $time_start;
        echo "Script done in $time seconds\n";
    }

    public function clanInfo(){
        $time_start = microtime(true); 
        echo "<pre>";
            print_r($this->data_model->clanInfo($this->input->get('name')));
        echo "</pre>";
        $time_end = microtime(true);
        $time = $time_end - $time_start;
        echo "Script done in $time seconds\n";
    }

    public function getAllOnlineList(){
        
        if(null!==$this->input->get('key') && $this->input->get('key')=='QMxFocIevlrMEfzr9Jt7F1lFlwv1rrN8'){

            /*geoloc(*/
            $apiKey ="4d8be4ed10aa4265a6cd78e79638757a";
            $ip = "175.158.225.193";
           
    

            $time_start = microtime(true); 
            $data=$this->data_model->getAllOnlineList();
            $rows="";
            $i=1;
            echo "<style>table{ border-collapse: collapse;}table, th, td {border: 1px solid black;}</style>";
            $temp=[];

            foreach($data as $p){
                $temp[]=(object)[
                    "id"=>$p['CT_Data']->UserID,
                    "name"=>$p['Aggregate_Data']->name,
                    "class"=>$p['Aggregate_Data']->class,
                    "level"=>$p['Aggregate_Data']->level,
                    "ip"=>$p['CT_Data']->IP
                ];
            }

            usort($temp,function($first,$second){
                return $first->ip < $second->ip;
            });

            
            foreach($temp as $p){
                $location = $this->get_geolocation($apiKey, $p->ip);
                $decodedLocation = json_decode($location, true);
                $rows.="<tr>"
                     ."<td>{$i}</td>"
                     ."<td>{$p->id}</td>"
                     ."<td>{$p->name}</td>"
                     ."<td>{$p->class}</td>"
                     ."<td>{$p->level}</td>"
                     ."<td>{$p->ip}</td>"
                     ."<td><img src='{$decodedLocation['country_flag']}' style='width:50%'/></td>"
                     ."</tr>";
                $i++;
            }

            echo "<table><thead><th>&nbsp;</th><th>Username</th><th>Character</th><th>Class</th><th>Level</th><th>IP</th>";
            echo "<tbody>{$rows}</tbody></table>";

            $time_end = microtime(true);
            $time = $time_end - $time_start;
            echo "Script done in $time seconds\n";
        }
       
    }

    private function get_geolocation($apiKey, $ip, $lang = "en", $fields = "*", $excludes = "") {
        $url = "https://api.ipgeolocation.io/ipgeo?apiKey=".$apiKey."&ip=".$ip;
        $cURL = curl_init();
        curl_setopt($cURL, CURLOPT_URL, $url);
        curl_setopt($cURL, CURLOPT_HTTPGET, true);
        curl_setopt($cURL, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($cURL, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Accept: application/json'
        ));
        //return curl_exec($cURL);
        return file_get_contents($url);
    }


    public function getBlessOwner(){
        echo "<pre>";
            print_r($this->data_model->getBlessOwner());
        echo "</pre>";
    }

    public function getOnlineList(){
        echo "<pre>";
            print_r($this->data_model->getOnlineList());
        echo "</pre>";
    }

    public function getServerStatus(){
        echo "<pre>";
            print_r($this->data_model->serverStatus());
        echo "</pre>";
    }

    public function getSODClanList(){
        echo "<pre>";
            print_r($this->data_model->getSODClanList());
        echo "</pre>";
    }

    public function getClanList(){
        echo "<pre>";
             print_r($this->data_model->getClanList());
        echo "</pre>";
    }

    public function updateRanking(){
        $time_start = microtime(true); 
        echo "<pre>";
            print_r($this->data_model->updatePlayerRanking());
        echo "</pre>";
        $time_end = microtime(true);
        $time = $time_end - $time_start;
        echo "Script done in $time seconds\n";
    }

    public function initializeWebsiteData(){
        echo "depricated";
        exit;
    }


    public function initializePlayerRanking(){
        $time_start = microtime(true); 
            $this->data_model->updatePlayerRanking(2);
        $time_end = microtime(true);
        $time = $time_end - $time_start;
        echo "Script done in $time seconds\n";
    }

    public function initializeItemData(){
        $time_start = microtime(true); 
            //$this->data_model->updatePlayerRanking(2);
            $this->data_model->initializeItemList();
        $time_end = microtime(true);
        $time = $time_end - $time_start;
        echo "Script done in $time seconds\n";
    }

    
}
