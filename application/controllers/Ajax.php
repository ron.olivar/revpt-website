<?php
class Ajax extends CI_Controller {

    function __construct(){
        parent::__Construct();
        $this->load->helper('url');
        $this->load->library('recaptcha');
        $this->load->model('data_model');
        date_default_timezone_set("Asia/Kuala_Lumpur");
    }

    public function getOnlineList(){
            echo json_encode($this->data_model->getOnlineList());
    }

    public function getSODClanList(){
        echo "<pre>";
            print_r($this->data_model->getSODClanList());
        echo "</pre>";
    }

    public function getClanList(){
        echo "<pre>";
             print_r($this->data_model->getClanList());
        echo "</pre>";
    }


}
