<div class="col-lg-4">
				<aside class="pt-sidebar pt-sidebar-right pt-sidebar-sticky">
						<div class="pt-widget pt-widget-highlighted">
							<h4 class="pt-widget-title"><span><span class="text-main-1">Server</span> Status</span></h4>
							<div class="pt-widget-content">
								<div class="pt-widget-post">
										<span class="pt-post-image">
											<img class='shadowfilter' src="<?php echo base_url(); ?>assets/images/<?php echo $server_status; ?>.png" alt="">
										</span>
										<h3 class="pt-post-title"><span id='serverTime'>Loading</span></h3>
										<p>
											<span id="serverDate">Loading</span>
											<br/>
											<?php 
												$noun=count($online_players)>1?"Players":"Player";
												$noun=count($online_players)==0?"":$noun;
											?>
											<?php echo anchor('pages/list-online',"<span class='fa fa-eye'></span> ".count($online_players)."</strong> ".$noun." Online","class='text-success online-count'"); ?>
												
										</p>					
									</div>
							</div>
						</div>

						<div class="pt-widget pt-widget-highlighted">
							<div class="pt-widget-content">
								<!--<iframe data-aa="1154374" src="//acceptable.a-ads.com/1154374" scrolling="no" style="border:0px; padding:0; overflow:hidden" allowtransparency="true"></iframe> 
								-->
								<script type="text/javascript" src="https://adhitzads.com/1041611"></script>
							</div>
						</div>

						<div class="pt-widget pt-widget-highlighted">
							<h4 class="pt-widget-title"><span><span class="text-main-1">Bless</span> Castle</span></h4>
							<div class="pt-widget-content">
								<div class="pt-widget-post text-center">
										<span class="pt-post-image">
											<img class='shadowfilter' src="<?php echo base_url(); ?>assets/images/bless.png" alt="">
										</span>
										<h3 class="pt-post-title">
											Current Owner
										</h3>
										<span>
											<img src="<?php echo $bless->logo."?t=".time(); ?>"/> <?php echo $bless->name; ?>
										</span>
								</div>
								<hr/>
								<div class='bg-dark-3 text-center'>
										<small class='text-white'>Time until next seige</small>
										<ul class='bc'>
											<li><span><i class='fa fa-hourglass-half text-warning'></i></span>&nbsp;</li>
											<li><span class='text-warning' id="bcDays">0</span>days</li>
											<li><span class='text-warning' id="bcHours">0</span>Hours</li>
											<li><span class='text-warning' id="bcMinutes">0</span>Minutes</li>
											<li><span class='text-warning' id="bcSeconds">0</span>Seconds</li>
										</ul>
								</div>										
							</div>
						</div>	  
						
						<div class="pt-widget pt-widget-highlighted">
							<h4 class="pt-widget-title"><span><span class="text-main-1">Top 3</span> SOD</span></h4>
							<div class="pt-widget-content">
								
								<?php $x=1; foreach($sod_clans as $sod){ ?>

									<div class="pt-widget-post">
										<span class="pt-post-image">
											<img class='shadowfilter' src="<?php echo base_url(); ?>assets/images/bel-<?php echo $x;?>.png" alt="">
										</span>
										<h3 class="pt-post-title"><span><img src="<?php echo $sod->logo."?t=".time();?>"/> <?php echo $sod->name;?></span></h3>
										<p><?php echo $sod->score;?> Pts</p>
									</div>

								<?php $x++; if($x>3)break;} ?>
								
								
									
									
							</div>
						</div>

						<div class="pt-widget pt-widget-highlighted">
							<h4 class="pt-widget-title"><span><span class="text-main-1">Community</span> Discussion</span></h4>
							<div class="pt-widget-content">
								<ul class="pt-social-links-3 pt-social-links-cols-4">
									<li><a class="pt-social-flickr" href="https://discord.gg/9FgC8Qf" target="_blank"><span class="fab fa-discord"></span></a></li>
									<li><a class="pt-social-facebook" href="https://web.facebook.com/revolutionpt2019" target="_blank"><span class="fab fa-facebook" target="_blank"></span></a></li>
									<li><a class="pt-social-facebook" href="https://web.facebook.com/groups/334356770713659/" target="_blank"><span class="fab fa-facebook" target="_blank"></span></a></li>
									<li><a class="pt-social-youtube" href="https://www.youtube.com/channel/UCpgkZ3NE8ZOCCk6nVfQ0lng" target="_blank"><span class="fab fa-youtube" target="_blank"></span></a></li>
								</ul>
							</div>
						</div>
				</aside>
			</div>