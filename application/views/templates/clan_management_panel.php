<div  class="pt-widget pt-widget-highlighted clan_management_panel">
    <h4 class="pt-widget-title"><span><span class="text-main-1">Manage</span> <?php echo $ClanName; ?></span></h4>
    <div class="pt-widget-content">
        <form id='form-clan-details-<?php echo $MIconCnt;?>' enctype="multipart/form-data" action="#" class="pt-form pt-form-gg clan-details" novalidate='novalidate'>
            <div class="pt-form-response-success"></div>
            <div class="pt-form-response-error"></div>
            <div class="row vertical-gap sm-gap">
                <div class="col-md-6">
                    <div class="pt-feature-2">
                        <div class="pt-feature-icon">
                            <img class='logo-preview' src="<?php echo CLAN_IMAGE_URL.$MIconCnt.".bmp?t=".time();?>"/>
                        </div>
                        <div class="pt-feature-cont text-center">
                            <h3 class="pt-feature-title"><?php echo $ClanName; ?></h3>
                            <div class="pt-gap-1"></div>
                            <span class='clan-note note-preview'>
                                <?php echo $Note; ?>
                            </span>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 form-inputs">
                    <div class="form-group">
                        <label for="logo">Clan Logo</label>
                        <input type="file" name="logo" class='form-control ignore' id="logo-<?php echo $MIconCnt;?>"/>
                        <small>gif,png,jpg,bmp file <= 512kb</small>
                    </div>
                    <div class="pt-gap"></div>
                    <div class="form-group">
                        <label for="notice">Clan Note</label>
                        <textarea id='inputCN-<?php echo $MIconCnt;?>' data-id='<?php echo $MIconCnt;?>' maxlength="80"  class="form-control required note-input ignore" name="notice" rows="3" placeholder="Clan Note"></textarea>
                    </div>
                    <div id="the-count">
                        <span class='charcnt' id="curcnt-<?php echo $MIconCnt;?>">0</span>
                        <span id="maximum">/ 80</span>
                    </div>
                    <div class="pt-gap-1"></div>
                    <input name='clanid' type='hidden' value='<?php echo $MIconCnt;?>'/>
                    <button type='submit' class="pt-btn pt-btn-rounded pt-btn-color-dark-3">
                        <span>Save</span>
                    </button>
                </div>
            </div>   
        </form>
        <div class="panel-heading" role="tab" id="accordion-<?php echo $MIconCnt;?>-heading">
            <a class="collapsed" data-toggle="collapse" data-parent="#accordion-1" href="#accordion-<?php echo $MIconCnt;?>" aria-expanded="false" aria-controls="accordion-1-2">
                Members <span class="panel-heading-arrow fa fa-angle-down"></span>
            </a>
        </div>
        <div id='accordion-<?php echo $MIconCnt;?>' class="panel-collapse collapse" role="tabpanel" aria-labelledby="accordion-<?php echo $MIconCnt;?>-heading">
            <table class='table table-dark table-striped table-hover ranking-table' style="margin-bottom:10px">
                <thead>
                        <tr>
                            <th width='10%'>&nbsp;</th>
                            <th width='20%'>&nbsp;</th>
                            <th width='50%'>&nbsp;</th>
                            <th width='20%'>&nbsp;</th>
                        </tr>
                </thead>
                <tbody>
                    <?php $x=1; foreach($members as $member){ ?>
                        <tr>
                            <td><?php echo $x; ?></td>
                            <td><img class="char-thumb-xs" src="<?php echo base_url(); ?>assets/images/characters/<?php echo $member->type;?>.png"/></td>
                            <td><?php echo $member->name;?></td>
                            <td class='text-right' style='padding-right:1rem;'>Lvl <strong class='text-main-6'><?php echo $member->level;?></strong></td>
                        </tr>
                    <?php $x++; } ?>
                </tbody>
            </table>    
        </div>
    </div>
</div>
<script>
  
  $('#inputCN-<?php echo $MIconCnt;?>').keyup();
</script>