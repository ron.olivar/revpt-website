<!--navbar-->
<nav class="pt-navbar pt-navbar-top pt-navbar-sticky pt-navbar-autohide">
        <div class="container">
            <div class="pt-nav-table">
                
                <?php echo anchor('',"<img class='shadowfilter' src='".base_url()."assets/images/logo.png' alt='RevPT' width='199'>","class='pt-nav-logo'") ?>
            
                <ul class="pt-nav pt-nav-right d-none d-lg-table-cell" data-nav-mobile="#pt-nav-mobile">
                    <li><?php echo anchor('',"Home") ?></li>
                    <li class=" pt-drop-item"><?php echo anchor('pages/upgrade-mixing',"Game Guide") ?>
                        <ul class="dropdown">         
                                <li><?php echo anchor('pages/upgrade-mixing',"Mixing Table") ?></li>
                                <li><?php echo anchor('pages/items-weapons',"Weapons") ?></li>
                                <li><?php echo anchor('pages/items-defenses',"Defenses") ?></li>
                                <li><?php echo anchor('pages/items-accessories',"Accessories") ?></li>
                        </ul>
                    </li>
                    <li class=" pt-drop-item"><?php echo anchor('pages/ranking-level',"Rankings") ?>
                        <ul class="dropdown">         
                                <li><?php echo anchor('pages/ranking-bellatra-clan',"SOD Clan Ranking") ?></li>
                                <li><?php echo anchor('pages/ranking-bellatra-solo',"SOD Solo Ranking") ?></li>
                                <li><?php echo anchor('pages/ranking-level',"Level Ranking") ?></li>
                                <li><?php echo anchor('pages/ranking-clan',"Clan Ranking") ?></li>
                        </ul>
                    </li>
                    <li><?php echo anchor('pages/donate',"Donate") ?></li>
                    <li><?php echo anchor('pages/account',"Account") ?></li>
                    <li><?php echo anchor('pages/download',"Download") ?></li>
                </ul>
                <ul class="pt-nav pt-nav-right pt-nav-icons">       
                        <li class="single-icon d-lg-none">
                            <a href="#" class="no-link-effect" data-nav-toggle="#pt-nav-mobile">
                                <span class="pt-icon-burger">
                                    <span class="pt-t-1"></span>
                                    <span class="pt-t-2"></span>
                                    <span class="pt-t-3"></span>
                                </span>
                            </a>
                        </li>
                </ul>
            </div>
        </div>
    </nav>
<!--/navbar-->
</header>

<!--mobilenav-->   
<div id="pt-nav-mobile" class="pt-navbar pt-navbar-side pt-navbar-right-side pt-navbar-overlay-content d-lg-none">
    <div class="nano">
        <div class="nano-content">
          <?php echo anchor('',"<img src='".base_url()."assets/images/logo.png' alt='RevPT' width='120'>","class='pt-nav-logo'") ?>
            <div class="pt-navbar-mobile-content">
                <ul class="pt-nav">
                    <!-- menu from [data-mobile-menu="#pt-nav-mobile"] -->
                </ul>
            </div>
        </div>
    </div>
</div>
<!--/mobilenav -->
<!--
<div class="pt-gap-2"></div> 
<div class='container'>
<div class='row'>
 <div class='col text-center'>
     <span class='bg-white'>
        <iframe data-aa="1154374" src="//ad.a-ads.com/1154374?size=990x90" scrolling="no" style="width:990px; height:90px; border:0px; padding:0; overflow:hidden" allowtransparency="true"></iframe>
    </span>
 </div>
</div>
</div>
-->