<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>RevolutionPT</title>

    <meta name="description" content="Revolution Pristontale - Custom EXP Rate, Max Level 150, Daily/Hunt/Begginer/Boss Quests, 10 Classes up to tier 5">
    <meta name="keywords" content="game, gaming, pristontale, priston tale, priston">
    <meta name="author" content="_pt">

    <link rel="icon" type="image/png" href="<?php echo base_url(); ?>assets/images/favicon.png">

    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700%7cOpen+Sans:400,700" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/vendor/bootstrap/dist/css/bootstrap.min.css">
    <script defer src="<?php echo base_url(); ?>assets/vendor/fontawesome-free/js/all.js"></script>
    <script defer src="<?php echo base_url(); ?>assets/vendor/fontawesome-free/js/v4-shims.js"></script>
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/vendor/ionicons/css/ionicons.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/vendor/flickity/dist/flickity.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/vendor/photoswipe/dist/photoswipe.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/vendor/photoswipe/dist/default-skin/default-skin.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/vendor/bootstrap-slider/dist/css/bootstrap-slider.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/vendor/summernote/dist/summernote-bs4.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/goodgames.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/custom.css">
    
</head>

<body>
<div id="bg"></div>
<header class="pt-header pt-header-opaque">
<!--contacts-->
<div class="pt-contacts-top">
    <div class="container">
        <div class="pt-contacts-left">
            <ul class="pt-social-links">
                <li><a class="pt-social-flickr" href="https://discord.gg/9FgC8Qf" target="_blank"><span class="fab fa-discord"></span></a></li>
                <li><a class="pt-social-facebook"  href="https://web.facebook.com/revolutionpt2019"><span class="fab fa-facebook"></span></a></li>
                <li><a class="pt-social-facebook" href="https://web.facebook.com/groups/334356770713659/"><span class="fab fa-facebook"></span></a></li>
                <li><a class="pt-social-youtube" href="https://www.youtube.com/channel/UCpgkZ3NE8ZOCCk6nVfQ0lng"><span class="fab fa-youtube"></span></a></li>
            </ul>
        </div>
        <div class="pt-contacts-right">
            <ul class="pt-contacts-icons">
            </ul>
        </div>
    </div>
</div>
<!--/contacts-->
