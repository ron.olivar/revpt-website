<div class="pt-gap-4"></div>
			
            <!-- START: Footer -->
            <footer class="pt-footer">
        
                <div class="container">
                    
                </div>
        
                <div class="pt-copyright">
                    <div class="container">
                        <div class="pt-copyright-left">
                            &copy; 2019 <a href="#" target="_blank">RevolutionPT</a>
                        </div>
                        <div class="pt-copyright-right">
                            <ul class="pt-social-links-2">
                                <li><a class="pt-social-flickr" href="https://discord.gg/9FgC8Qf" target="_blank"><span class="fab fa-discord"></span></a></li>
                                <li><a class="pt-social-facebook"  href="https://web.facebook.com/revolutionpt2019"><span class="fab fa-facebook"></span></a></li>
                                <li><a class="pt-social-facebook" href="https://web.facebook.com/groups/334356770713659/"><span class="fab fa-facebook"></span></a></li>
                                <li><a class="pt-social-youtube" href="https://www.youtube.com/channel/UCpgkZ3NE8ZOCCk6nVfQ0lng"><span class="fab fa-youtube"></span></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </footer>
            <!-- END: Footer -->
        </div>
        <!--/pt-main-->
        
            <!-- START: Page Background -->
            <img class="pt-page-background-top" src="<?php echo base_url(); ?>assets/images/bg-top.jpg" alt="">
            <img class="pt-page-background-bottom" src="<?php echo base_url(); ?>assets/images/bg-bottom.png" alt="">
           
            <!-- END: Page Background -->
        
            
        <script src="<?php echo base_url(); ?>assets/vendor/jquery/dist/jquery.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/vendor/object-fit-images/dist/ofi.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/vendor/gsap/src/minified/TweenMax.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/vendor/gsap/src/minified/plugins/ScrollToPlugin.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/vendor/bootstrap/dist/js/bootstrap.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/vendor/sticky-kit/dist/sticky-kit.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/vendor/jarallax/dist/jarallax.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/vendor/imagesloaded/imagesloaded.pkgd.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/vendor/flickity/dist/flickity.pkgd.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/vendor/photoswipe/dist/photoswipe.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/vendor/photoswipe/dist/photoswipe-ui-default.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/vendor/jquery-validation/dist/jquery.validate.js"></script>
        <script src="<?php echo base_url(); ?>assets/vendor/moment/min/moment.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/vendor/moment-timezone/builds/moment-timezone-with-data.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/vendor/hammerjs/hammer.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/vendor/nanoscroller/jquery.nanoscroller.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/vendor/bootstrap-slider/dist/bootstrap-slider.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/plugins/pt-share/pt-share.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/revpt.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/goodgames-init.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/particles.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/bg.js"></script>
        <?php echo $this->recaptcha->getScriptTag(); ?>
       

        <style>
            .ranking-invert{
                background:#2C3034;
            }
        </style>

        <script>

            $(document).ready(function(){

                $('#ranking-class-filter button').click(function(){
                    $('#ranking-class-filter button').removeClass('btn-primary').addClass('btn-secondary');
                    $(this).removeClass('btn-secondary');
                    $(this).addClass('btn-primary');
                    charClass=$(this).attr('data-class');

                    $('.ranking-table').removeClass('table-striped');
                    $('.ranking-rows tr').removeClass('ranking-invert').hide();
                    
                    if(charClass=='all'){
                        $('.ranking-rows tr').each(function(i){
                            $(this).children('td').slice(0, 1).html(i+1);
                            $(this).show();
                        });
                        $('.ranking-table').addClass('table-striped');
                    }else{
                        $("img[alt="+charClass+"]").each(function(i){

                            $(this).closest('tr').children('td').slice(0,1).html(i+1);
                            if(i%2==0){
                                $(this).closest('tr').addClass('ranking-invert').show();
                            }else{
                                $(this).closest('tr').show();
                            }
                            
                        });
                    }
                    
                });


                $('#spec button').click(function(){
                    $('#spec button').removeClass('btn-primary').addClass('btn-secondary');
                    $(this).removeClass('btn-secondary');
                    $(this).addClass('btn-primary');
               
                    var specVariations = new Array();
                    specVariations[0] = new Array(1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00);
                    specVariations[1] = new Array(0.75, 0.85, 0.80, 0.90, 1.00, 1.00, 1.15, 1.25);
                    specVariations[2] = new Array(0.80, 0.85, 0.80, 0.90, 1.00, 1.00, 1.15, 1.25);
                    specVariations[3] = new Array(1.10, 1.15, 0.80, 0.85, 1.00, 1.00, 0.80, 0.85);
                    specVariations[4] = new Array(1.05, 1.15, 0.85, 0.90, 1.05, 1.10, 0.75, 0.85);
                    specVariations[5] = new Array(0.80, 0.85, 1.15, 1.25, 0.85, 0.90, 0.80, 0.85);
                    specVariations[6] = new Array(1.05, 1.15, 0.80, 0.90, 1.00, 1.00, 0.75, 0.85);
                    specVariations[7] = new Array(0.80, 0.85, 1.15, 1.20, 0.85, 0.90, 0.80, 0.85);
                    specVariations[8] = new Array(1.10, 1.15, 0.80, 0.85, 1.00, 1.00, 0.75, 0.85);
                    specVariations[9] = new Array(0.80, 0.85, 1.15, 1.20, 0.85, 0.90, 0.80, 0.85);
                    specVariations[10] = new Array(0.80, 0.85, 0.80, 0.90, 1.00, 1.00, 1.15, 1.25);

                    $('#spec button').removeClass('btn-primary').addClass('btn-secondary');
                    $(this).removeClass('btn-secondary');
                    $(this).addClass('btn-primary');

                    var spec=$(this).text();

                    $("ul.item-attributes-req li").each(function(){
                        content=$(this).html();
                        attribute=content.substr(0, content.indexOf(' &nbsp;'));
                        value=$(this).find('span').text();
                        specNS=$(this).attr('data-ns');

                        mod=0;
                        switch(spec){
                            case 'NS': mod=0; break;
                            case 'AS': mod=1; break;
                            case 'ATS': mod=2; break;
                            case 'FS': mod=3; break;
                            case 'KS': mod=4; break;
                            case 'MGS': mod=5; break;
                            case 'MS': mod=6; break;
                            case 'PRS': mod=7; break;
                            case 'PS': mod=8; break;
                            case 'SHA': mod=9; break;
                            case 'ASS': mod=10; break;
                        }
                        
                        attr=-1; //level
                        switch(attribute){
                            case 'Strength':attr=0; break;
                            case 'Spirit':attr=2; break;
                            case 'Talent':attr=4; break;
                            case 'Agility':attr=6; break;
                        }

                        if(mod==0){
                            $(this).html(attribute+" &nbsp;<span>"+specNS+"</span>");
                        }else{
                            if(attr!=-1){
                                range=parseInt(specNS * specVariations[mod][attr])+" - "+parseInt(specNS * specVariations[mod][attr+1]);
                                $(this).html(attribute+" &nbsp;<span>"+range+"</span>");
                            }
                        }
                    });
                });


                if($("ul.item-attributes-req li")){
                    $("ul.item-attributes-req li").each(function(){
                    content=$(this).html();
                    attribute=content.substr(0, content.indexOf(' &nbsp;'));
                    value=$(this).find('span').text();
                    $(this).attr('data-ns',value);  
                    });
                }


                if($('.online-count')){

                    setInterval(
                        function(){
                            $.ajax({
                                url:'<?php echo site_url();?>/ajax/getOnlineList',
                                type:"post",
                                success: function(response){
                                    d = JSON.parse(response);
                                    var noun=d.length>1?"Players":"Player";
                                    noun=d.length==0?"":noun;
                                    $('.online-count').html("<span class='fa fa-eye'></span> "+d.length+" "+noun+" Online");
                                },error: function(e){
                                    console.log(e);
                                }
                            });
                        }
                       
                    , <?php echo REFRESH_RATE;?>);
                }

                if($('#list_online')){

                    setInterval(
                        function(){
                            $.ajax({
                                url:'<?php echo site_url();?>/ajax/getOnlineList',
                                type:"post",
                                success: function(response){
                                    d = JSON.parse(response);
                                    list="";

                                    $.each( d, function( i, val ) {
                                        list+="<tr>"
                                            +"<td>"+(i+1)+"</td>"
                                            +"<td><div class='img-td-wrapper'>"
                                            +"      <img class='char-thumb-xs' src='<?php echo base_url(); ?>assets/images/characters/"+val.class+".png' alt='"+val.class+"'>"
                                            +"    </div>"
                                            +"</td>"
                                            +"<td class='text-left'>"
                                            +"    <span>&nbsp;"+val.name+"</span>"
                                            +"</td>"
                                            +"<td><strong  class='text-main-6'>"+val.level+"</strong></td>"
                                            +"</tr>";
                                    });
                                    $('#list_online').hide().html(list).fadeIn(150);
                                },error: function(e){
                                    console.log(e);
                                }
                            });
                        }
                    
                    , <?php echo REFRESH_RATE;?>);
                }



                


                if($('#clanmgmt')){
                    $('#clanmgmt').on('keyup','.clan-details .note-input',function(){
                        var characterCount = $(this).val().length;
                        var dataid=$(this).attr('data-id');
                        $('#curcnt-'+dataid).text(characterCount);
                    });

                    $('#clanmgmt').on('submit','.clan-details',function(){  
                        form=$(this);
                        data= new FormData(this);
                        controls=$(this).find('div .form-inputs');
                        formcontrols=controls.html();
                        successArea=$(this).find('.pt-form-response-success');
                        errorArea=$(this).find('.pt-form-response-error');
                        logo=$(this).find('.logo-preview');
                        note=$(this).find('.note-preview');
                        noteIn=$(this).find('.note-input');

                        successArea.hide();
                        errorArea.hide();

                        controls.html("<p><i class='fa fa-spinner fa-spin'></i> Updating. Please do not leave this page.</p>");
                        setTimeout(function(){
                                $.ajax({
                                    url:'<?php echo site_url();?>/forms/updateClan',
                                    type:"post",
                                    data:data, 
                                    processData:false,
                                    contentType:false,
                                    cache:false,
                                    async:false,
                                    success: function(response){

                                        d = JSON.parse(response);
                                        if(d.type && d.type === 'success'){
                                            successArea.html(d.response).show();
                                            controls.html(formcontrols);
                                            
                                            respData=d.data;
                                            console.log(respData);

                                            if(respData.image){
                                               logo.attr('src',respData.image);
                                            }
                                            
                                            note.html(respData.note);
                                            

                                        }else{
                                            errorArea.html(d.response).show();
                                            controls.html(formcontrols);
                                        }
                                    
                                    },error: function(e){
                                        errorArea.html(e).show();
                                        controls.html(formcontrols);
                                    }
                            });
                        }, 500);

                        
                        return false;
                    });
                }
            });


            $("#form-clan").submit(function(){
                
                var responseSuccess =$(this).find('.pt-form-response-success');
                var responseError = $(this).find('.pt-form-response-error');
                var form = $(this);

                console.log(form.children());
                
                $.ajax({
                    type: 'POST',
                    url: '<?php echo site_url();?>/forms/manageClan',
                    data: form.serialize(),
                    success: function success(response) {
                        response = JSON.parse(response);
                        if (response.type && response.type === 'success') {
                            form.remove();
                            $('#clanmgmt').html(response.response).show();
                            
                        } else {
                            
                            responseSuccess.hide();
                            responseError.html(response.response).show();
                        }
                        
                    },
                    error: function error(response) {
                        console.log(response);
                        //responseSuccess.hide();
                       // responseError.html(response.responseText).show();
                    }
                });
                
              
              return false;

            });


            (function() {
                moment.locale('en');
                 var deviceTime,serverTime,actualTime,timeOffset;

                // Run each second lap 
                var updateDisplay = function() {
                    $('#serverTime').html(actualTime.tz('<?php echo GAME_TIMEZONE; ?>').format('h:mm:ss a ([GMT] z)'));
                    $('#serverDate').html(actualTime.tz('<?php echo GAME_TIMEZONE; ?>').format('dddd MMMM D, YYYY'));
                    blessTime();
                };

                var blessTime =function(){
                    const dayINeed = <?php echo BCDAY; ?>;
                    const bcStartTime="<?php echo BCTIME; ?>";
                    const today = actualTime.tz('<?php echo GAME_TIMEZONE; ?>').isoWeekday();
                    bcStart = today <= dayINeed? moment().isoWeekday(dayINeed).format('MM-DD-YYYY '+bcStartTime) : moment().add(1, 'weeks').isoWeekday(dayINeed).format('MM-DD-YYYY '+bcStartTime);
                   
                    _start=moment.tz(bcStart,"MM-DD-YYYY HH:mm:ss",'<?php echo GAME_TIMEZONE; ?>');
                    timeTillNextStart=moment.duration(_start.diff(actualTime));


                    if(timeTillNextStart.asSeconds()>0) {
                            var d = timeTillNextStart.days(),
                                h = timeTillNextStart.hours(),
                                m = timeTillNextStart.minutes(),
                                s = timeTillNextStart.seconds();


                            d = $.trim(d).length === 1 ? '0' + d : d;
                            h = $.trim(h).length === 1 ? '0' + h : h;
                            m = $.trim(m).length === 1 ? '0' + m : m;
                            s = $.trim(s).length === 1 ? '0' + s : s;

                            // show how many hours, minutes and seconds are left

                            $("#bcDays").html(d);
                            $("#bcHours").html(h);
                            $("#bcMinutes").html(m);
                            $("#bcSeconds").html(s);
                    }else{
                            $("#bcDays").html("00");
                            $("#bcHours").html("00");
                            $("#bcMinutes").html("00");
                            $("#bcSeconds").html("00");
                    }


                }

                var timerHandler = function() {
                    // Get current time on the device
                    actualTime = moment();
                    // Add the calculated offset
                    actualTime.add(timeOffset);
                    updateDisplay();
                    // Re-run this next second wrap
                    setTimeout(timerHandler, (1000 - (new Date().getTime() % 1000)));
                };


                var fetchServerTime = function() {
                    var xmlhttp = new XMLHttpRequest();
                    xmlhttp.onload = function() {
                        var dateHeader = xmlhttp.getResponseHeader('Date');
                        serverTime = moment(new Date(dateHeader)); 
                        
                        // Store the differences between device time and server time
                        timeOffset = serverTime.diff(moment());
                        
                        // Now when we've got all data, trigger the timer for the first time
                        timerHandler();
                    }
                    xmlhttp.open("HEAD", window.location.href);
                    xmlhttp.send();
                }
                    // Trigger the whole procedure
                fetchServerTime();
            })();


            $(".toggle-password").click(function() {
                var input = $(this).closest('.input-group').find('input');
                console.log(input);
                if (input.attr("type") == "password") {
                    input.attr("type", "text");
                    $(this).html("Hide <i class='fa fa-eye-slash'></i>");
                    $(this).removeClass("pt-btn-color-dark-3");
                    $(this).addClass("pt-btn-color-dark-1");
                } else {
                    input.attr("type", "password");
                    $(this).html("Show <i class='fa fa-eye'></i>");
                    $(this).removeClass("pt-btn-color-dark-1");
                    $(this).addClass("pt-btn-color-dark-3");
                }
            });

        </script>

        
        </body>
        </html>
        