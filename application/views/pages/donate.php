<div class="pt-main">
	<div class="pt-gap-2"></div> 
	<!--container-->
	<div class="container">
        <div class="row vertical-gap">
            <div class="col-lg-8">
                <!--maincontent-->
                        <div class="pt-widget pt-widget-highlighted">
							<h4 class="pt-widget-title"><span><span class="text-main-1">Donate</span></span></h4>
							<div class="pt-widget-content">
                            <p class='text-justify'><strong class='text-white'>Our server is totally free</strong>, but to keep us online 24hrs, we need your help.
                            There are several donation options/methods, choose the one that pleases you the most.</p>
                                

                            <!-- START: Accordion -->
                            <div class="pt-accordion" id="accordion-1" role="tablist" aria-multiselectable="true">
                                <div class="panel panel-default">
                                    <div class="panel-heading" role="tab" id="accordion-1-1-heading">
                                        <a data-toggle="collapse" data-parent="#accordion-1" href="#accordion-1-1" aria-expanded="true" aria-controls="accordion-1-1">
                                            Electronic Transfer <span class="panel-heading-arrow fa fa-angle-down"></span>
                                        </a>
                                    </div>
                                    <div id="accordion-1-1" class="panel-collapse collapse show" role="tabpanel" aria-labelledby="accordion-1-1-heading">
                                        <table class='pt-table' style="margin-bottom:10px">
                                            <tbody>
                                                <tr>
                                                    <td width='20%'>Paypal</td>
                                                    <td><a href="https://paypal.me/botro">Botro</a></td>
                                                    <td>aquariusbotro@gmail.com</td>
                                                </tr>
                                                <tr>
                                                    <td>Coins.ph</td>
                                                    <td><a href="https://coins.ph/m/join/3h3wu4">Genaray Corbeta</a></td>
                                                    <td>genecorbeta09@gmail.com</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="panel panel-default">
                                    <div class="panel-heading" role="tab" id="accordion-1-2-heading">
                                        <a class="collapsed" data-toggle="collapse" data-parent="#accordion-1" href="#accordion-1-2" aria-expanded="false" aria-controls="accordion-1-2">
                                            Money Remittance <span class="panel-heading-arrow fa fa-angle-down"></span>
                                        </a>
                                    </div>
                                    <div id="accordion-1-2" class="panel-collapse collapse" role="tabpanel" aria-labelledby="accordion-1-2-heading">
                                         <table class='pt-table' style="margin-bottom:10px">
                                            <tbody>
                                            	<tr>
                                                    <td width="20%">Western Union</td>
                                                    <td rowspan='4'>John Paul B. Tac-an</td>
                                                    <td rowspan='4'>Bliss House 49, Matti, Digos City, Davao del Sur, 8002, Philippines</td>
                                                    <td rowspan='4'>+639298779425</td>
                                                </tr>
                                                <tr>
                                                    <td>M.Lhullier</td>
                                                    
                                                </tr>
                                                <tr>
                                                    <td>LBC</td>
                                                    
                                                </tr>
                                                <tr>
                                                    <td>Palawan Express</td>
                                                    
                                                </tr>
                                                
                                            </tbody>
                                        </table>    
                                    </div>
                                </div>
                                <div class="panel panel-default">
                                    <div class="panel-heading" role="tab" id="accordion-1-3-heading">
                                        <a class="collapsed" data-toggle="collapse" data-parent="#accordion-1" href="#accordion-1-3" aria-expanded="false" aria-controls="accordion-1-3">
                                            Cryptocurrency <span class="panel-heading-arrow fa fa-angle-down"></span>
                                        </a>
                                    </div>
                                    <div id="accordion-1-3" class="panel-collapse collapse" role="tabpanel" aria-labelledby="accordion-1-3-heading">
                                    <table class='pt-table' style="margin-bottom:10px">
                                        <tbody>
                                                <tr>
                                                    <td width='20%'>Bitcoin</td>
                                                    <td>3QoKf79momHe2oJzh8seDNGQPisLfnx6z8</td>
                                                </tr>
                                                <tr>
                                                    <td>Ethereum</td>
                                                    <td>0x68b56f2472ad479cb7fcb8c16d27b9c2149433ab</td>
                                                </tr>
                                                <tr>
                                                    <td>Litecoin</td>
                                                    <td>LhwFkVfwrCPMbaZCRLrnXRKLaR1zEeR3hD</td>
                                                </tr>
                                                <tr>
                                                    <td>Dogecoin</td>
                                                    <td>D6VwMMfiwHgAhG8eLL94Em9N2GHtZmnsFe</td>
                                                </tr>
                                               
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <!-- END: Accordion -->

                            <p class='text-justify'>
                                Donation proceeds go directly towards the improvement and maintenance of the server such as payment to the hosting provider and the
                                acquisition of other resources and future developments. Donors are given rewards as tokens of appreciation. Please contact us via 
                                our social channels before donating.
                            </p>
                            <p>
                                <strong class='text-white'>Thank you for your continued support.</strong> 
                            </p>

							</div><!--/pt-widget-content-->
						</div>


                <!--/maincontent-->
            </div>
            <!--sidebar-->
                <?php
                    $this->load->view('templates/sidebar');
                ?>
            <!--/sidebar-->

        </div>
    </div>
    <!--/container-->
