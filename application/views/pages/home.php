<div class="pt-main">
	<div class="pt-gap"></div> 
	<!--container-->
	<div class="container">
		<?php if($mainSlider){ ?>
		<!--image slider-->
		<div class="pt-image-slider" data-autoplay="5000">
			<div class="pt-image-slider-item">
				<img src="<?php echo base_url(); ?>assets/images/slide-1.jpg" alt="" class="pt-image-slider-img" data-thumb="assets/images/slide-1-thumb.jpg">
				
				<div class="pt-image-slider-content" style="opacity: 0;">
					
						<h3 class="h4">Slide 1</h3>
						<p class="text-white">As we passed, I remarked a beautiful church-spire rising above some old elms in the park; and before them, in the midst of a lawn, chimneys covered with ivy, and the windows shining in the sun.</p>
					
					
				</div>
				
			</div>
			
			<div class="pt-image-slider-item">
				<img src="<?php echo base_url(); ?>assets/images/slide-2.jpg" alt="" class="pt-image-slider-img" data-thumb="assets/images/slide-2-thumb.jpg">
				
				<div class="pt-image-slider-content" style="opacity: 0;">
					
						<h3 class="h4">Slide 2</h3>
						<p class="text-white">Now the races of these two have been for some ages utterly extinct, and besides to discourse any further of them would not be at all to my purpose. But the concern I have most at heart is for our Corporation of Poets, from whom I am preparing a petition to your Highness,  to be subscribed with the names of one...</p>
						
					
				</div>
				
			</div>
			
			<div class="pt-image-slider-item">
				<img src="<?php echo base_url(); ?>assets/images/slide-4.jpg" alt="" class="pt-image-slider-img" data-thumb="assets/images/slide-3-thumb.jpg">
				<div class="pt-image-slider-content" style="opacity: 0;">
					
					<h3 class="h4">Slide 3</h3>
					<p class="text-white">Now the races of these two have been for some ages utterly extinct, and besides to discourse any further of them would not be at all to my purpose. But the concern I have most at heart is for our Corporation of Poets, from whom I am preparing a petition to your Highness,  to be subscribed with the names of one...</p>
					
				
				</div>
				
			</div>

			
			
        </div>
		<div class="pt-gap-2"></div>

		<!--/image slider-->
		<?php } ?>


		<div class="row vertical-gap">
			<!--maincontent-->
			<div class="col-lg-8">
            <!-- START: Tabbed News  -->
            <h3 class="pt-decorated-h-2"><span><span class="text-main-1">News &amp;</span> Announcements</span></h3>
            
            <div class="pt-tabs">
											<ul class="nav nav-tabs nav-tabs-fill" role="tablist">
													<li class="nav-item">
															<a class="nav-link active" href="#tabs-1-1" role="tab" data-toggle="tab">General</a>
													</li>
													<li class="nav-item">
															<a class="nav-link" href="#tabs-1-2" role="tab" data-toggle="tab">Events</a>
													</li>
													<li class="nav-item">
															<a class="nav-link" href="#tabs-1-3" role="tab" data-toggle="tab">Top Players</a>
													</li>
													<li class="nav-item">
															<a class="nav-link" href="#tabs-1-4" role="tab" data-toggle="tab">Ranking</a>
													</li>
											</ul>
											<div class="tab-content">
												<div role="tabpanel" class="tab-pane fade show active" id="tabs-1-1"><!--General Tab -->
													<div class="pt-gap"></div>


													<?php
													  /** Featured posts */ 
														foreach($articles as $post){ 
															if($post->category=='F'){
													?>
													
														<div class="pt-blog-post pt-blog-post-border-bottom">
																<span class="pt-post-img">
																	<img src="<?php echo base_url(); ?>assets/images/posts/<?php echo $post->image; ?>">
																</span>
																<div class="pt-gap-1"></div>
																<h2 class="pt-post-title h4"><span><?php echo $post->title; ?></span></h2>
																<div class="pt-post-date mt-10 mb-10">
																		<span class="fa fa-calendar"></span>
																		<?php 
																			$start = strlen($post->date_start)?date_format(date_create_from_format('Y-m-d', $post->date_start),'M j, Y'):"";
																			$end = strlen($post->date_end)?date_format(date_create_from_format('Y-m-d', $post->date_end),'M j, Y'):"";
																			
																			echo $start;
																			echo strlen($end)?" - ".$end:$end;
																		?>
																</div>
																<div class="pt-post-text">
																		<p><?php echo $post->content; ?></p>
																</div>
														</div>
													<?php }}
														/**End: Featured posts */
													?>

													
													
													<?php
													  /** General posts */ 
														foreach($articles as $post){ 
															if($post->category=='G'){
													?>

														<div class="pt-blog-post pt-blog-post-border-bottom">
																<div class="row vertical-gap">
																		<div class="col-lg-3 col-md-5">
																				<a href="#" class="pt-post-img">
																						<img src="<?php echo base_url(); ?>assets/images/posts/<?php echo $post->image; ?>">
																						
																						<span class="pt-post-categories">
																								<span class="bg-main-1"><?php echo $post->label; ?></span>
																						</span>
																				</a>
																		</div>
																		<div class="col-lg-9 col-md-7">
																				<h2 class="pt-post-title h4"><span><?php echo $post->title; ?></span></h2>
																				<div class="pt-post-date mt-10 mb-10">
																					<span class="fa fa-calendar"></span> 
																					<?php 
																						$start = strlen($post->date_start)?date_format(date_create_from_format('Y-m-d', $post->date_start),'M j, Y'):"";
																						$end = strlen($post->date_end)?date_format(date_create_from_format('Y-m-d', $post->date_end),'M j, Y'):"";
																						
																						echo $start;
																						echo strlen($end)?" - ".$end:$end;
																					?>
																				</div>
																				<div class="pt-post-text">
																						<p>
																							<?php echo $post->content; ?>
																						</p>
																				</div>
																		</div>
																</div>
														</div>

													<?php }}
														/**End: General posts */
													?>
													
													

													

													
													<div class="pt-gap"></div>
												</div><!--/General Tab -->

												<div role="tabpanel" class="tab-pane fade" id="tabs-1-2"><!--Events Tab -->
														<div class="pt-gap"></div>	
														<div class="pt-blog-post pt-blog-post-border-bottom">
															<span class="pt-post-img">
																	<img src="<?php echo base_url(); ?>assets/images/events.jpg" alt="Grab your sword and fight the Horde">
															</span>
															<div class="pt-gap-1"></div>
															<h2 class="pt-post-title h4"><span>Grab your sword and fight the Horde!</span></h2>
															<div class="pt-post-text">
																	<p>Check out the planned events for the upcoming weeks. This in addition to nightly GM events!</p>
															</div>
														</div>					
														
														<?php
													  /** Event posts */ 
														foreach($articles as $post){ 
															if($post->category=='E'){
													?>

														<div class="pt-blog-post pt-blog-post-border-bottom">
																<div class="row vertical-gap">
																		<div class="col-lg-3 col-md-5">
																				<a href="#" class="pt-post-img">
																						<img src="<?php echo base_url(); ?>assets/images/posts/<?php echo $post->image; ?>">
																						
																						<span class="pt-post-categories">
																								<span class="bg-main-3"><?php echo $post->label; ?></span>
																						</span>
																				</a>
																		</div>
																		<div class="col-lg-9 col-md-7">
																				<h2 class="pt-post-title h4"><span><?php echo $post->title; ?></span></h2>
																				<div class="pt-post-date mt-10 mb-10">
																					<span class="fa fa-calendar"></span> 
																					<?php 
																						$start = strlen($post->date_start)?date_format(date_create_from_format('Y-m-d', $post->date_start),'M j, Y'):"";
																						$end = strlen($post->date_end)?date_format(date_create_from_format('Y-m-d', $post->date_end),'M j, Y'):"";
																						
																						echo $start;
																						echo strlen($end)?" - ".$end:$end;
																					?>
																				</div>
																				<div class="pt-post-text">
																						<p>
																							<?php echo $post->content; ?>
																						</p>
																				</div>
																		</div>
																</div>
														</div>

													<?php }}
														/**End: Event posts */
													?>



														<div class="pt-gap"></div>
												</div><!--/events tab-->
													
												<div role="tabpanel" class="tab-pane fade" id="tabs-1-3"><!-- START: Top-Players -->
												 <div class="pt-gap"></div>
												 <table class="table table-dark table-striped  ranking-table">
														<thead>
																<tr>
																		<th colspan="3"><span class="fa fa-trophy text-warning"></span> Top-in-Class Characters</th>
																</tr>
														</thead>
														<tbody>
															<?php foreach($topclass as $top){ ?>
																<tr>
																		<td class="text-center">
																			<div class='img-td-wrapper'>
																				<img class="circle char-thumb" src="<?php echo base_url(); ?>assets/images/characters/<?php echo $top->class;?>.png" alt="<?php echo $top->class;?>">
																			</div>
																		</td>
																		<td><?php echo $top->title;?> <span class='text text-warning'><?php echo $top->name;?></span></td>
																		<td class="text-center"><strong class='text-main-6'><?php echo $top->level;?></strong></td>
																</tr>
															<?php } ?>

															
														</tbody>
												</table>		

												<div class="pt-gap"></div>
													</div><!--/Topplayers Tab -->

													<div role="tabpanel" class="tab-pane fade" id="tabs-1-4"><!--Rankings Tab-->
													<div class="pt-gap"></div>
													<table class="table table-dark table-striped table-hover ranking-table">
															<thead>
																	<tr>
																			<th colspan="4">Top 10 Level Ranking</th>
																	</tr>
															</thead>
															<tbody>
																<?php $x=1; foreach($toplist as $player){ ?>
																	<tr>
																			<td><?php echo $x;?></td>
																			<td class="text-center"><div class='img-td-wrapper'>
																				<img class="char-thumb-xs" src="<?php echo base_url(); ?>assets/images/characters/<?php echo $player->class;?>.png" alt="<?php echo $player->class;?>"></div>
																			</td>
																			<td><?php echo $player->name;?></td>
																			<td class="text-center"><strong class='text-main-6'><?php echo $player->level;?></strong></td>
																	</tr>
																<?php $x++; } ?>
																
															</tbody>
													</table>		
													<div class="pt-gap"></div>
													<div class="text-center">
														<a href="<?php echo site_url('pages/ranking-level'); ?>" class="center pt-btn pt-btn-rounded pt-btn-outline pt-btn-color-danger">
																<span>See All</span>
																<span class="icon"><i class="fa fa-eye"></i></span>
														</a>
													</div>
													</div><!--/Rankings Tab -->		
                </div><!--end Tab content-->
            </div>
            <!-- News and Announcements -->


            <!-- START: Latest Pictures -->
            <div class="pt-gap"></div>
            <h2 class="pt-decorated-h-2 h3"><span><span class="text-main-1">Media</span> </span></h2>
            <div class="pt-gap"></div>
            <div class="pt-popup-gallery" data-pswp-uid="1">
                <div class="row vertical-gap">
                    <div class="col-lg-4 col-md-6">
                        <div class="pt-gallery-item-box">
                            <a href="<?php echo base_url(); ?>assets/images/gallery-1.jpg" class="pt-gallery-item" data-size="640x480">
                                <div class="pt-gallery-item-overlay"><span class="ion-eye"></span></div>
                                <img src="<?php echo base_url(); ?>assets/images/gallery-1.jpg" alt="">
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6">
                        <div class="pt-gallery-item-box">
                            <a href="<?php echo base_url(); ?>assets/images/gallery-2.jpg" class="pt-gallery-item" data-size="640x480">
                                <div class="pt-gallery-item-overlay"><span class="ion-eye"></span></div>
                                <img src="<?php echo base_url(); ?>assets/images/gallery-2.jpg" alt="">
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6">
                        <div class="pt-gallery-item-box">
                            <a href="<?php echo base_url(); ?>assets/images/gallery-3.jpg" class="pt-gallery-item" data-size="640x480">
                                <div class="pt-gallery-item-overlay"><span class="ion-eye"></span></div>
                                <img src="<?php echo base_url(); ?>assets/images/gallery-3.jpg" alt="">
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6">
                        <div class="pt-gallery-item-box">
                            <a href="<?php echo base_url(); ?>assets/images/gallery-4.jpg" class="pt-gallery-item" data-size="640x480">
                                <div class="pt-gallery-item-overlay"><span class="ion-eye"></span></div>
                                <img src="<?php echo base_url(); ?>assets/images/gallery-4.jpg" alt="">
                            </a>
                        </div>
					</div>
					<div class="col-lg-4 col-md-6">
                        <div class="pt-gallery-item-box">
                            <a href="<?php echo base_url(); ?>assets/images/gallery-5.jpg" class="pt-gallery-item" data-size="640x480">
                                <div class="pt-gallery-item-overlay"><span class="ion-eye"></span></div>
                                <img src="<?php echo base_url(); ?>assets/images/gallery-5.jpg" alt="">
                            </a>
                        </div>
					</div>
					<div class="col-lg-4 col-md-6">
                        <div class="pt-gallery-item-box">
                            <a href="<?php echo base_url(); ?>assets/images/gallery-6.jpg" class="pt-gallery-item" data-size="640x480">
                                <div class="pt-gallery-item-overlay"><span class="ion-eye"></span></div>
                                <img src="<?php echo base_url(); ?>assets/images/gallery-6.jpg" alt="">
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <!--/Latest Pictures -->
			</div>   
			<!--/maincontent-->
			<!--sidebar-->
			 <?php
			    $this->load->view('templates/sidebar');
			  ?>
			<!--/sidebar-->
		</div>
	</div>
<!--/container-->