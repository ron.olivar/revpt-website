<div class="pt-main">
	<div class="pt-gap-2"></div> 
	<!--container-->
	<div class="container">
        <div class="row vertical-gap">
            <div class="col-lg-8">
                <!--maincontent-->
                        <div class="pt-widget pt-widget-highlighted">
							<h4 class="pt-widget-title"><span>SOD&nbsp;<span class="text-main-1"><?php echo date('F');?></span> Solo Ranking</span></h4>
							<div class="pt-widget-content">
                            <table class="table table-dark table-striped table-hover ranking-table">
                                <thead>
                                        <tr>
                                            <th width='10%'>Rank</th>
                                            <th width='40%'>Character</th>
                                            <th width='20%'>Date</th>
                                            <th width='30%'>Score</th>
                                        </tr>
                                </thead>
                                <tbody class='text-center'>
                                        <?php $x=1; foreach($list as $rec){ ?>
                                            <tr>
                                                <td><?php echo $x;?></td>
                                                <td>
                                                    <div class='img-td-wrapper text-left'>
                                                        <img class="char-thumb-xs" src="<?php echo base_url(); ?>assets/images/characters/<?php echo $rec->type;?>.png"/>
                                                        <span>&nbsp;<?php echo $rec->name;?></span>
                                                    </div>
                                                    
                                                </td>
                                                <td class='text-left'><?php echo $rec->date;?></td>
                                                <td class='text-right' style='padding-right:1rem;'><strong class='text-main-6'><?php echo $rec->score;?></strong> Pts</td>
                                            </tr>
                                        <?php $x++;} ?>
                                        
                                      
                                </tbody>
                        </table> 
                                
							</div><!--/pt-widget-content-->
						</div>


                <!--/maincontent-->
            </div>
            <!--sidebar-->
                <?php
                    $this->load->view('templates/sidebar');
                ?>
            <!--/sidebar-->

        </div>
    </div>
    <!--/container-->
