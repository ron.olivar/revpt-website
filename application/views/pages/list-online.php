<div class="pt-main">
	<div class="pt-gap-2"></div> 
	<!--container-->
	<div class="container">
        <div class="row vertical-gap">
            <div class="col-lg-8">
                <!--maincontent-->
                        <div class="pt-widget pt-widget-highlighted">
							<h4 class="pt-widget-title"><span><span class="text-main-1">Online</span> Players</span></h4>
							<div class="pt-widget-content">
                            <table class="table table-dark table-striped table-hover ranking-table">
                                <thead>
                                        <tr>
                                            <th>&nbsp;</th>
                                            <th>Class</th>
                                            <th style='text-align:left'>Character</th>
                                            <th>Level</th>
                                        </tr>
                                </thead>
                                <tbody id='list_online' class='text-center'>
                                    <?php $x=1; foreach($online_players as $player){?>
                                        <tr>
                                            <td><?php echo $x; ?></td>
                                            <td>
                                                <div class='img-td-wrapper'>
                                                    <img class="char-thumb-xs" src="<?php echo base_url(); ?>assets/images/characters/<?php echo $player->class;?>.png" alt="<?php echo $player->class;?>">
                                                </div>
                                            </td>
                                            <td class='text-left'>
                                                <span>&nbsp;<?php echo $player->name;?></span>
                                            </td>
                                            <td><strong class='text-main-6'><?php echo $player->level;?></strong></td>
                                        </tr>
                                    <?php $x++;}?>
                                        
                                      
                                </tbody>
                        </table> 
                                
							</div><!--/pt-widget-content-->
						</div>


                <!--/maincontent-->
            </div>
            <!--sidebar-->
                <?php
                    $this->load->view('templates/sidebar');
                ?>
            <!--/sidebar-->

        </div>
    </div>
    <!--/container-->
