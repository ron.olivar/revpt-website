<div class="pt-main">
	<div class="pt-gap-2"></div> 
	<!--container-->
	<div class="container">
        <div class="row vertical-gap">
            <div class="col-lg-8">
                <!--maincontent-->
                        <div class="pt-widget pt-widget-highlighted">
							<h4 class="pt-widget-title"><span><span class="text-main-1">Account</span> Management</span></h4>
							<div class="pt-widget-content">
                            <div class='pt-tabs'>
                                    <ul class="nav nav-tabs nav-tabs-fill min-menu" role="tablist">
                                        <li class="nav-item">
												<a class="nav-link active" href="#register" role="tab" data-toggle="tab">Register</a>
										</li>
                                        <li class="nav-item">
                                                <a class="nav-link" href="#cpassword" role="tab" data-toggle="tab">Change Password</a>
                                        </li>
                                        <li class="nav-item">
                                                <a class="nav-link" href="#clanmgmt" role="tab" data-toggle="tab">Clan Management</a>
                                        </li>
                                       
									</ul>
                                    <hr/>
                                    <div class="tab-content"> 
                                        <div role="tabpanel" class="tab-pane fade show active" id="register">
                                            <form id='form-register'  class="pt-form pt-form-ajax pt-form-gg" action='<?php echo site_url();?>/forms/register' method='post' autocomplete="off" >
                                                
                                                <div class="pt-form-response-success"></div>
                                                <div class="pt-form-response-error"></div>
                                                
                                                
                                                <div class="row vertical-gap sm-gap">
                                                    <div class="col-md-6">
                                                        <input type="text" class="form-control required" name="username" placeholder="Username">

                                                        <div class="input-group" style="margin-bottom:10px">
                                                            <input type="password" value="" name="password" class="required form-control" placeholder="Password">
                                                            <button type='button' class="toggle-password pt-btn pt-btn-color-dark-3">Show <i class='fa fa-eye'></i></button>
                                                        </div>
                                                        <input type="email" class="form-control required" name="email" placeholder="Email">
                                                    </div>
                                                    <div class="col-md-6">
                                                        <input type="text" class="form-control required" name="firstname" placeholder="Firstname">
                                                        <input type="text" class="form-control required" name="lastname" placeholder="Lastname">
                                                        <?php  echo $this->recaptcha->getWidget(array('data-theme' => 'dark')); ?>
                                                    </div>
                                                </div>
                                                <div class='pt-gap'></div>
                                                        <hr/>
                                                <div class='row vertical-gap sm-gap'>
                                                    <div class='col-md-8'>
                                                        &nbsp;
                                                    </div>
                                                    <div class='col-md-4 text-right'>
                                                       <button type='submit' class="pt-btn  btn-primary pt-btn-lg">
                                                            <span>Register</span>
                                                        </button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                       
                                        <div role="tabpanel" class="tab-pane fade" id="cpassword">

                                            <form id='form-password' action='<?php echo site_url();?>/forms/changePW' class="pt-form pt-form-ajax pt-form-gg" autocomplete="off">
                                                <div class="pt-form-response-success"></div>
                                                <div class="pt-form-response-error"></div>
                                            
                                                <div class="row vertical-gap sm-gap">
                                                    <div class="col-md-6">
                                                        <input type="text" class="form-control required" name="username" placeholder="Username">

                                                        <div class="input-group" style="margin-bottom:10px">
                                                            <input type="password" value="" name="oldpassword" class="required form-control" placeholder="Old Password">
                                                            <button type='button' class="toggle-password pt-btn pt-btn-color-dark-3">Show <i class='fa fa-eye'></i></button>
                                                        </div>

                                                        <div class="input-group" style="margin-bottom:10px">
                                                            <input type="password" value="" name="newpassword" class="required form-control" placeholder="New Password">
                                                            <button type='button' class="toggle-password pt-btn pt-btn-color-dark-3">Show <i class='fa fa-eye'></i></button>
                                                        </div>
                                                        
                                                    </div>
                                                    <div class="col-md-6">
                                                        <?php  echo $this->recaptcha->getWidget(array('data-theme' => 'dark')); ?> 
                                                    </div>
                                                </div>
                                                
                                                <div class='row vertical-gap sm-gap'>
                                                    <div class='col text-right'>
                                                        <div class='pt-gap'></div>
                                                        <hr/>
                                                        <button type='submit' class="pt-btn  btn-primary pt-btn-lg">
                                                            <span>Update Password</span>
                                                        </button>
                                                    </div>
                                                </div>

                                                
                                            </form>
                                        </div>
                                        
                                        <div role="tabpanel" class="tab-pane fade" id="clanmgmt">
                                            <form id='form-clan' action='#' class="pt-form pt-form-gg" autocomplete="off">
                                                <div class="pt-form-response-success"></div>
                                                <div class="pt-form-response-error"></div>
                                                 <div class="row vertical-gap sm-gap">
                                                    <div class="col-md-6">
                                                        <input type="text" class="form-control required" name="username" placeholder="Username">

                                                        <div class="input-group" style="margin-bottom:10px">
                                                            <input type="password" value="" name="password" class="required form-control" placeholder="Password">
                                                            <button type='button' class="toggle-password pt-btn pt-btn-color-dark-3">Show <i class='fa fa-eye'></i></button>
                                                        </div>

                                                    </div>
                                                    <div class="col-md-6">
                                                        <?php  echo $this->recaptcha->getWidget(array('data-theme' => 'dark')); ?> 
                                                    </div>
                                                </div>
                                                
                                                <div class='row vertical-gap sm-gap'>
                                                    <div class='col text-right'>
                                                        <div class='pt-gap'></div>
                                                        <hr/>
                                                        <button type='submit' class="pt-btn  btn-primary pt-btn-lg">
                                                            <span>Manage Clan</span>
                                                        </button>
                                                    </div>
                                                </div>

                                                
                                            </form>
										</div>
									
                                    </div>   
                                </div>
							</div><!--/pt-widget-content-->
                        </div>
                        

                <!--/maincontent-->
            </div>
            <!--sidebar-->
                <?php
                    $this->load->view('templates/sidebar');
                ?>
            <!--/sidebar-->

        </div>
    </div>
    <!--/container-->
