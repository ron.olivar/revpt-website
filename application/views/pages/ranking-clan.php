<div class="pt-main">
	<div class="pt-gap-2"></div> 
	<!--container-->
	<div class="container">
        <div class="row vertical-gap">
            <div class="col-lg-8">
                <!--maincontent-->
                        <div class="pt-widget pt-widget-highlighted">
							<h4 class="pt-widget-title"><span><span class="text-main-1">Clan</span> Ranking</span></h4>
							<div class="pt-widget-content">


                            
                                <div class="pt-info-box pt-info-box-noicon">
                                    <div class="pt-info-box-close pt-info-box-close-btn">
                                        <i class="ion-close-round"></i>
                                    </div>
                                    <em>Showing only clans that have atleast <?php echo LIST_CLANMIN; ?> members.</em>
                                </div>

                            <table class="table table-dark table-striped table-hover ranking-table">
                                <thead>
                                        <tr>
                                            <th width='10%'>Rank</th>
                                            <th width='30%'>Clan</th>
                                            <th width='20%'></th>
                                            <th width='30%'>Chief</th>
                                            <th width='10%'>Members</th>
                                        </tr>
                                </thead>
                                <tbody class='text-center'>
                                <?php $x=1; 
                                        foreach($clans as $clan){
                                ?>
                                    <tr>
                                        <td><?php echo $x;?></td>
                                        <td>
                                            <div class='img-td-wrapper text-left'>
                                                <img class="char-thumb-xs" src="<?php echo $clan->logo."?t=".time();?>" alt="<?php echo $clan->name."-logo";?>">
                                                <span>&nbsp;<?php echo $clan->name;?></span>
                                            </div>
                                        </td>
                                        <td class='text-left'>
					                        <?php if($clan->castle){ ?><img class="char-thumb-xs shadowfilter" src="<?php echo base_url(); ?>assets/images/bless.png" alt="BC"><?php }?>
					                        <?php if($clan->sod){ ?><img class="char-thumb-xs shadowfilter" src="<?php echo base_url(); ?>assets/images/bel-<?php echo $clan->sod; ?>.png" alt="SOD"><?php }?>
			                            </td>
                                         <td>
                                            <div class='img-td-wrapper text-left'>
                                                <img class="char-thumb-xs" src="<?php echo base_url(); ?>assets/images/characters/<?php echo $clan->chief_class;?>.png" alt="<?php echo $clan->chief_class;?>">
                                                <span>&nbsp;<?php echo $clan->chief;?></span>
                                            </div>
                                        </td>
                                        <td><strong><?php echo $clan->members;?></strong></td>
                                    </tr>

                                <?php $x++; } ?>

                                </tbody>
                            </table> 
							</div><!--/pt-widget-content-->
						</div>


                <!--/maincontent-->
            </div>
            <!--sidebar-->
                <?php
                    $this->load->view('templates/sidebar');
                ?>
            <!--/sidebar-->

        </div>
    </div>
    <!--/container-->
