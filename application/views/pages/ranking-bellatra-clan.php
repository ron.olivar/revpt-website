<div class="pt-main">
	<div class="pt-gap-2"></div> 
	<!--container-->
	<div class="container">
        <div class="row vertical-gap">
            <div class="col-lg-8">
                <!--maincontent-->
                        <div class="pt-widget pt-widget-highlighted">
							<h4 class="pt-widget-title"><span>SOD&nbsp;<span class="text-main-1">Clan</span> Ranking</span></h4>
							<div class="pt-widget-content">
                            <table class="table table-dark table-striped table-hover ranking-table">
                                <thead>
                                        <tr>
                                            <th>Rank</th>
                                            <th>Clan</th>
                                            <th>Chief</th>
                                            <th>Score</th>
                                        </tr>
                                </thead>
                                <tbody class='text-center'>
                                        <?php $x=1; foreach($sod_clans as $clan){ ?>
                                            <tr>
                                                <td><?php echo $x; ?></td>
                                                <td>
                                                    <div class='img-td-wrapper text-left'>
                                                        <img class="char-thumb-xs" src="<?php echo $clan->logo."?t=".time(); ?>" alt="clan logo">
                                                        <span>&nbsp;<?php echo $clan->name;?></span>
                                                    </div>
                                                </td>
                                                <td>
                                                    <div class='img-td-wrapper text-left'>
                                                        <img class="char-thumb-xs" src="<?php echo base_url(); ?>assets/images/characters/<?php echo $clan->chief_class;?>.png">
                                                        <span>&nbsp;<?php echo $clan->chief; ?></span>
                                                    </div>
                                                </td>
                                                <td class='text-right' style='padding-right:1rem;'><strong class='text-main-6'><?php echo $clan->score; ?></strong> Pts</td>
                                             </tr>
                                        
                                        <?php $x++;}?>
                                </tbody>
                        </table> 
                                
							</div><!--/pt-widget-content-->
						</div>


                <!--/maincontent-->
            </div>
            <!--sidebar-->
                <?php
                    $this->load->view('templates/sidebar');
                ?>
            <!--/sidebar-->

        </div>
    </div>
    <!--/container-->
