<div class="pt-main">
	<div class="pt-gap-2"></div> 
	<!--container-->
	<div class="container">
        <div class="row vertical-gap">
            <div class="col-lg-8">
                <!--maincontent-->
                        <div class="pt-widget pt-widget-highlighted">
							<h4 class="pt-widget-title"><span><span class="text-main-1">Download</span></span></h4>
							<div class="pt-widget-content text-center">
                                <h4 class="text-white">Download Links</h4>
                                <div class='row vertical-gap vertical-gap-sm'>
                                    <div class='col-md-6'>  
                                        
                                            <div class="pt-feature-2">
                                                <div class="pt-feature-icon">
                                                     <a href="https://mega.nz/#!BooXwSzQ!WuClLvBYkUPctdymPGgGScsKh2-OV7gLxA0o90kYH3c" target="_blank">
                                                         <img class='shadowfilter' src="<?php echo base_url();?>assets/images/download-mega.png" alt=""/>
                                                    </a>
                                                </div>
                                                <div class="pt-feature-cont text-center">
                                                    <h3 class="pt-feature-title"><span class="text-main-1">Mega</span></h3>
                                                    Download Mirror 1
                                                </div>
                                            </div>
                                       
                                    </div>
                                    <div class='col-md-6'> 
                                        
                                            <div class="pt-feature-2">
                                                <div class="pt-feature-icon">
                                                    <a href="http://www.mediafire.com/file/ikcukqyvwgt8p91/Revolution_2019_Full_Client_Installer.exe/file" target="_blank">
                                                        <img class='shadowfilter' src="<?php echo base_url();?>assets/images/download-mediafire.png" alt=""/>
                                                    </a>
                                                </div>
                                                <div class="pt-feature-cont text-center">
                                                    <h3 class="pt-feature-title"><span class="text-main-1">Media</span>Fire</h3>
                                                    Download Mirror 2
                                                </div>
                                            </div>
                                        
                                    </div>
                                    
                                </div>
                                <p class='text-white'>Full Client Installer - Version: 8301 - Size: 1.31GB</p>
                                <p>Note: DirectX Installer will prompt after you install the Full Client, install it!</p>    
                                <p>Important Note! You might have some trouble installing the game because your antivirus 
                                    would detect the game with Malware; Refer to this <a href='https://web.facebook.com/napp04/videos/10218305117320169/'>video</a> to fix it.</p>
                                <div class='pt-gap-2'></div>
                                <hr/>
                                <h4 class="text-white">System Requirements</h4>
                                <div class="pt-info-box text-success">
                                    <div class="pt-info-box-icon">
                                        <i class="ion-checkmark-round"></i>
                                    </div>
                                    <h3></h3>
                                    <em>Pristontale is a game from 2002 and can run on most modern PCs today.</em>
                                </div>
                                <table class="table table-dark table-striped requirements-table">
                                    <thead>
                                        <tr>
                                            <th>&nbsp;</th>
                                            <th>Minimum</th>
                                            <th>Recommended</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>OS</td>
                                            <td>Windows XP</td>
                                            <td>Windows XP/Vista/7/8/10</td>
                                        </tr>
                                        <tr>
                                            <td>CPU</td>
                                            <td>Intel Pentium IV or Equivalent</td>
                                            <td>Intel Core 2 Quad 2.0 or higher</td>
                                        </tr>
                                        <tr>
                                            <td>RAM</td>
                                            <td>1GB</td>
                                            <td>2GB</td>
                                        </tr>
                                        <tr>
                                            <td>GPU</td>
                                            <td>Any 256MB - 3D accelerated card</td>
                                            <td>Any 1GB - 3D accelerated card</td>
                                        </tr>
                                        <tr>
                                            <td>DirectX</td>
                                            <td colspan='2'>DirectX 9.0c</td>
                                        </tr>
                                    </tbody>
                                </table>
                                <div class='pt-gap'></div>
                                
							</div><!--/pt-widget-content-->
						</div>


                <!--/maincontent-->
            </div>
            <!--sidebar-->
                <?php
                    $this->load->view('templates/sidebar');
                ?>
            <!--/sidebar-->

        </div>
    </div>
    <!--/container-->
