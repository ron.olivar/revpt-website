<div class="pt-main">
	<div class="pt-gap-2"></div> 
	<!--container-->
	<div class="container">
        <div class="row vertical-gap">
            <div class="col-lg-8">
                <!--maincontent-->
                        <div class="pt-widget pt-widget-highlighted">
							<h4 class="pt-widget-title"><span><span class="text-main-1">Mixing</span> Formulas</span></h4>
							<div class="pt-widget-content">
                                
                                <div class='pt-tabs'>
                                    <ul class="nav nav-tabs nav-tabs-fill" role="tablist">
                                        <li class="nav-item">
                                                <a class="nav-link active" href="#weapons" role="tab" data-toggle="tab">Weapons</a>
                                        </li>
                                        <li class="nav-item">
                                                <a class="nav-link" href="#armors" role="tab" data-toggle="tab">Armors/Robes</a>
                                        </li>
                                        <li class="nav-item">
                                                <a class="nav-link" href="#shields" role="tab" data-toggle="tab">Shields</a>
                                        </li>
                                        <li class="nav-item">
                                                <a class="nav-link" href="#orbs" role="tab" data-toggle="tab">Orbs</a>
                                        </li>
									</ul>

                                    <div class="tab-content">
                                    <div class="pt-gap"></div> 
                                        <div role="tabpanel" class="tab-pane fade show active" id="weapons">
                                            <table class="table table-dark table-striped table-hover mixing-table">
                                                <tr>
                                                    <th class='text-main-6' width='50%'>Required Sheltoms</th>
                                                    <th class='text-main-6' width='50%'>Additional Effects After Mixture</th>
                                                </tr>
                                                <?php foreach($formulas as $formula){
                                                        if($formula->type=='weapons'){
                                                ?>
                                                            <tr>
                                                                <td class='text-right text-main-6'>
                                                                    <ul>
                                                                        <?php if($formula->lucidy>0){ ?> <li><img class='shadowfilter' src="<?php echo base_url(); ?>assets/images/sheltoms/lucidy.png"/>x<?php echo $formula->lucidy;?></li> <?php } ?>
                                                                        <?php if($formula->sereneo>0){ ?> <li><img class='shadowfilter' src="<?php echo base_url(); ?>assets/images/sheltoms/sereneo.png"/>x<?php echo $formula->sereneo;?></li> <?php } ?>
                                                                        <?php if($formula->fadeo>0){ ?> <li><img class='shadowfilter' src="<?php echo base_url(); ?>assets/images/sheltoms/fadeo.png"/>x<?php echo $formula->fadeo;?></li> <?php } ?>
                                                                        <?php if($formula->sparky>0){ ?> <li><img class='shadowfilter' src="<?php echo base_url(); ?>assets/images/sheltoms/sparky.png"/>x<?php echo $formula->sparky;?></li> <?php } ?>
                                                                        <?php if($formula->radient>0){ ?> <li><img class='shadowfilter' src="<?php echo base_url(); ?>assets/images/sheltoms/radient.png"/>x<?php echo $formula->radient;?></li> <?php } ?>
                                                                        <?php if($formula->transparo>0){ ?> <li><img class='shadowfilter' src="<?php echo base_url(); ?>assets/images/sheltoms/transparo.png"/>x<?php echo $formula->transparo;?></li> <?php } ?>
                                                                        <?php if($formula->murky>0){ ?> <li><img class='shadowfilter' src="<?php echo base_url(); ?>assets/images/sheltoms/murky.png"/>x<?php echo $formula->murky;?></li> <?php } ?>
                                                                        <?php if($formula->devine>0){ ?> <li><img class='shadowfilter' src="<?php echo base_url(); ?>assets/images/sheltoms/devine.png"/>x<?php echo $formula->devine;?></li> <?php } ?>
                                                                        <?php if($formula->celesto>0){ ?> <li><img class='shadowfilter' src="<?php echo base_url(); ?>assets/images/sheltoms/celesto.png"/>x<?php echo $formula->celesto;?></li> <?php } ?>

                                                                    </ul>
                                                                </td>
                                                                <td class='text-center' style='padding:5px'><?php echo $formula->effect; ?></td>
                                                            </tr>
                                                <?php }} ?>
                                            </table>
                                        </div>
                                        <div role="tabpanel" class="tab-pane fade" id="armors">
                                            <table class="table table-dark table-striped table-hover mixing-table">
                                                <tr>
                                                    <th class='text-main-6' width='50%'>Required Sheltoms</th>
                                                    <th class='text-main-6' width='50%'>Additional Effects After Mixture</th>
                                                </tr>
                                                <?php foreach($formulas as $formula){
                                                        if($formula->type=='armors'){
                                                ?>
                                                            <tr>
                                                                <td class='text-right text-main-6'>
                                                                    <ul>
                                                                        <?php if($formula->lucidy>0){ ?> <li><img class='shadowfilter' src="<?php echo base_url(); ?>assets/images/sheltoms/lucidy.png"/>x<?php echo $formula->lucidy;?></li> <?php } ?>
                                                                        <?php if($formula->sereneo>0){ ?> <li><img class='shadowfilter' src="<?php echo base_url(); ?>assets/images/sheltoms/sereneo.png"/>x<?php echo $formula->sereneo;?></li> <?php } ?>
                                                                        <?php if($formula->fadeo>0){ ?> <li><img class='shadowfilter' src="<?php echo base_url(); ?>assets/images/sheltoms/fadeo.png"/>x<?php echo $formula->fadeo;?></li> <?php } ?>
                                                                        <?php if($formula->sparky>0){ ?> <li><img class='shadowfilter' src="<?php echo base_url(); ?>assets/images/sheltoms/sparky.png"/>x<?php echo $formula->sparky;?></li> <?php } ?>
                                                                        <?php if($formula->radient>0){ ?> <li><img class='shadowfilter' src="<?php echo base_url(); ?>assets/images/sheltoms/radient.png"/>x<?php echo $formula->radient;?></li> <?php } ?>
                                                                        <?php if($formula->transparo>0){ ?> <li><img class='shadowfilter' src="<?php echo base_url(); ?>assets/images/sheltoms/transparo.png"/>x<?php echo $formula->transparo;?></li> <?php } ?>
                                                                        <?php if($formula->murky>0){ ?> <li><img class='shadowfilter' src="<?php echo base_url(); ?>assets/images/sheltoms/murky.png"/>x<?php echo $formula->murky;?></li> <?php } ?>
                                                                        <?php if($formula->devine>0){ ?> <li><img class='shadowfilter' src="<?php echo base_url(); ?>assets/images/sheltoms/devine.png"/>x<?php echo $formula->devine;?></li> <?php } ?>
                                                                        <?php if($formula->celesto>0){ ?> <li><img class='shadowfilter' src="<?php echo base_url(); ?>assets/images/sheltoms/celesto.png"/>x<?php echo $formula->celesto;?></li> <?php } ?>

                                                                    </ul>
                                                                </td>
                                                                <td class='text-center' style='padding:5px'><?php echo $formula->effect; ?></td>
                                                            </tr>
                                                <?php }} ?>
                                            </table>
                                        </div>
                                        <div role="tabpanel" class="tab-pane fade" id="shields">
                                            <table class="table table-dark table-striped table-hover mixing-table">
                                                <tr>
                                                    <th class='text-main-6' width='50%'>Required Sheltoms</th>
                                                    <th class='text-main-6' width='50%'>Additional Effects After Mixture</th>
                                                </tr>
                                                <?php foreach($formulas as $formula){
                                                        if($formula->type=='shields'){
                                                ?>
                                                            <tr>
                                                                <td class='text-right text-main-6'>
                                                                    <ul>
                                                                        <?php if($formula->lucidy>0){ ?> <li><img class='shadowfilter' src="<?php echo base_url(); ?>assets/images/sheltoms/lucidy.png"/>x<?php echo $formula->lucidy;?></li> <?php } ?>
                                                                        <?php if($formula->sereneo>0){ ?> <li><img class='shadowfilter' src="<?php echo base_url(); ?>assets/images/sheltoms/sereneo.png"/>x<?php echo $formula->sereneo;?></li> <?php } ?>
                                                                        <?php if($formula->fadeo>0){ ?> <li><img class='shadowfilter' src="<?php echo base_url(); ?>assets/images/sheltoms/fadeo.png"/>x<?php echo $formula->fadeo;?></li> <?php } ?>
                                                                        <?php if($formula->sparky>0){ ?> <li><img class='shadowfilter' src="<?php echo base_url(); ?>assets/images/sheltoms/sparky.png"/>x<?php echo $formula->sparky;?></li> <?php } ?>
                                                                        <?php if($formula->radient>0){ ?> <li><img class='shadowfilter' src="<?php echo base_url(); ?>assets/images/sheltoms/radient.png"/>x<?php echo $formula->radient;?></li> <?php } ?>
                                                                        <?php if($formula->transparo>0){ ?> <li><img class='shadowfilter' src="<?php echo base_url(); ?>assets/images/sheltoms/transparo.png"/>x<?php echo $formula->transparo;?></li> <?php } ?>
                                                                        <?php if($formula->murky>0){ ?> <li><img class='shadowfilter' src="<?php echo base_url(); ?>assets/images/sheltoms/murky.png"/>x<?php echo $formula->murky;?></li> <?php } ?>
                                                                        <?php if($formula->devine>0){ ?> <li><img class='shadowfilter' src="<?php echo base_url(); ?>assets/images/sheltoms/devine.png"/>x<?php echo $formula->devine;?></li> <?php } ?>
                                                                        <?php if($formula->celesto>0){ ?> <li><img class='shadowfilter' src="<?php echo base_url(); ?>assets/images/sheltoms/celesto.png"/>x<?php echo $formula->celesto;?></li> <?php } ?>

                                                                    </ul>
                                                                </td>
                                                                <td class='text-center' style='padding:5px'><?php echo $formula->effect; ?></td>
                                                            </tr>
                                                <?php }} ?>
                                            </table>
                                        </div>
                                        <div role="tabpanel" class="tab-pane fade" id="orbs">
                                            <table class="table table-dark table-striped table-hover mixing-table">
                                                <tr>
                                                    <th class='text-main-6' width='50%'>Required Sheltoms</th>
                                                    <th class='text-main-6' width='50%'>Additional Effects After Mixture</th>
                                                </tr>
                                                <?php foreach($formulas as $formula){
                                                        if($formula->type=='orbs'){
                                                ?>
                                                            <tr>
                                                                <td class='text-right text-main-6'>
                                                                    <ul>
                                                                        <?php if($formula->lucidy>0){ ?> <li><img class='shadowfilter' src="<?php echo base_url(); ?>assets/images/sheltoms/lucidy.png"/>x<?php echo $formula->lucidy;?></li> <?php } ?>
                                                                        <?php if($formula->sereneo>0){ ?> <li><img class='shadowfilter' src="<?php echo base_url(); ?>assets/images/sheltoms/sereneo.png"/>x<?php echo $formula->sereneo;?></li> <?php } ?>
                                                                        <?php if($formula->fadeo>0){ ?> <li><img class='shadowfilter' src="<?php echo base_url(); ?>assets/images/sheltoms/fadeo.png"/>x<?php echo $formula->fadeo;?></li> <?php } ?>
                                                                        <?php if($formula->sparky>0){ ?> <li><img class='shadowfilter' src="<?php echo base_url(); ?>assets/images/sheltoms/sparky.png"/>x<?php echo $formula->sparky;?></li> <?php } ?>
                                                                        <?php if($formula->radient>0){ ?> <li><img class='shadowfilter' src="<?php echo base_url(); ?>assets/images/sheltoms/radient.png"/>x<?php echo $formula->radient;?></li> <?php } ?>
                                                                        <?php if($formula->transparo>0){ ?> <li><img class='shadowfilter' src="<?php echo base_url(); ?>assets/images/sheltoms/transparo.png"/>x<?php echo $formula->transparo;?></li> <?php } ?>
                                                                        <?php if($formula->murky>0){ ?> <li><img class='shadowfilter' src="<?php echo base_url(); ?>assets/images/sheltoms/murky.png"/>x<?php echo $formula->murky;?></li> <?php } ?>
                                                                        <?php if($formula->devine>0){ ?> <li><img class='shadowfilter' src="<?php echo base_url(); ?>assets/images/sheltoms/devine.png"/>x<?php echo $formula->devine;?></li> <?php } ?>
                                                                        <?php if($formula->celesto>0){ ?> <li><img class='shadowfilter' src="<?php echo base_url(); ?>assets/images/sheltoms/celesto.png"/>x<?php echo $formula->celesto;?></li> <?php } ?>

                                                                    </ul>
                                                                </td>
                                                                <td class='text-center' style='padding:5px'><?php echo $formula->effect; ?></td>
                                                            </tr>
                                                <?php }} ?>
                                            </table>
                                        </div>
                                    </div>   
                                </div>

							</div><!--/pt-widget-content-->
						</div>


                <!--/maincontent-->
            </div>
            <!--sidebar-->
                <?php
                    $this->load->view('templates/sidebar');
                ?>
            <!--/sidebar-->

        </div>
    </div>
    <!--/container-->
