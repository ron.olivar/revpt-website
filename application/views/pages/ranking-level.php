<div class="pt-main">
	<div class="pt-gap-2"></div> 
	<!--container-->
	<div class="container">
        <div class="row vertical-gap">
            <div class="col-lg-8">
                <!--maincontent-->
                        <div class="pt-widget pt-widget-highlighted">
							<h4 class="pt-widget-title"><span><span class="text-main-1">Level</span> Ranking</span></h4>
							<div class="pt-widget-content">

                                <div class="pt-info-box pt-info-box-noicon">
                                    <div class="pt-info-box-close pt-info-box-close-btn">
                                        <i class="ion-close-round"></i>
                                    </div>
                                    <em>Showing only players that are atleast <?php echo RANKING_MINLVL; ?> and have been active in the last <?php echo RANKING_LASTACTIVE; ?> days.</em>
                                </div>

                                <div class='row'>
                                        <div class='col'></div>
                                        <div class='col'>
                                            <div class="btn-toolbar" role="toolbar" aria-label="Toolbar with button groups">
                                                <div id='ranking-class-filter' class="btn-group btn-group-sm mr-2" role="group" aria-label="First group">
                                                    <button type="button" data-class="all" class="btn btn-primary">ALL</button>
                                                    <button type="button" data-class="fighter" class="btn btn-secondary">FS</button>
                                                    <button type="button" data-class="mechanician" class="btn btn-secondary">MS</button>
                                                    <button type="button" data-class="archer" class="btn btn-secondary">AS</button>
                                                    <button type="button" data-class="pikeman" class="btn btn-secondary">PS</button>
                                                    <button type="button" data-class="atalanta" class="btn btn-secondary">ATS</button>
                                                    <button type="button" data-class="knight" class="btn btn-secondary">KS</button>
                                                    <button type="button" data-class="magician" class="btn btn-secondary">MGS</button>
                                                    <button type="button" data-class="priestess" class="btn btn-secondary">PRS</button>
                                                    <button type="button" data-class="assassin" class="btn btn-secondary">ASS</button>
                                                    <button type="button" data-class="shaman" class="btn btn-secondary">SHA</button>
                                                </div>
                                            </div>
                                        </div>
                                        <div class='col'></div>
                                </div>
                                <div class='pt-gap'></div>

                               
                                <table class="table table-dark table-striped table-hover ranking-table">
                                <thead>
                                        <tr>
                                            <th>Rank</th>
                                            <th>Class</th>
                                            <th style='text-align:left'>Character</th>
                                            <th>Level</th>
                                        </tr>
                                </thead>
                                <tbody class='text-center ranking-rows'>
                                <?php $x=1;foreach($list as $data){ ?>
                                        <tr>
                                            <td><?php echo $x;?></td>
                                            <td>
                                                <div class='img-td-wrapper'>
                                                    <img class="char-thumb-xs" src="<?php echo base_url(); ?>assets/images/characters/<?php echo $data->class;?>.png" alt="<?php echo $data->class;?>">
                                                </div>
                                            </td>
                                            <td class='text-left'>
                                                <span>&nbsp;<?php echo $data->name;?></span>
                                            </td>
                                            <td><strong  class='text-main-6'><?php echo $data->level;?></strong></td>
                                        </tr>
                                <?php $x++;}?>

                                </tbody>
                            </table> 
                                
							</div><!--/pt-widget-content-->
						</div>


                <!--/maincontent-->
            </div>
            <!--sidebar-->
                <?php
                    $this->load->view('templates/sidebar');
                ?>
            <!--/sidebar-->

        </div>
    </div>
    <!--/container-->
