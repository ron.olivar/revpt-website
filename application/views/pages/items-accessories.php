<div class="pt-main">
	<div class="pt-gap-2"></div> 
	<!--container-->
	<div class="container">
        <div class="row vertical-gap">
            <div class="col-lg-8">
                <!--maincontent-->
                        <div class="pt-widget pt-widget-highlighted">
							<h4 class="pt-widget-title"><span><span class="text-main-1">Accesories </span>& misc.</h4>
							<div class="pt-widget-content">
							
                                <div class='pt-tabs'>
                                    <ul class="nav nav-tabs nav-tabs-fill min-menu" role="tablist">
                                        <li class="nav-item">
												<a class="nav-link active" href="#amulets" role="tab" data-toggle="tab">Amulets</a>
										</li>
                                        <li class="nav-item">
                                                <a class="nav-link" href="#rings" role="tab" data-toggle="tab">Rings</a>
                                        </li>
                                        <li class="nav-item">
                                                <a class="nav-link" href="#sheltoms" role="tab" data-toggle="tab">Sheltoms</a>
                                        </li>
									</ul>
									<hr/>
									<div class='row'>
										<div class='col'></div>
										<div class='col'>
											<div class="btn-toolbar" role="toolbar" aria-label="Toolbar with button groups">
												<div id='spec' class="btn-group btn-group-sm mr-2" role="group" aria-label="First group">
													<button type="button" class="btn btn-primary">NS</button>
													<button type="button" class="btn btn-secondary">AS</button>
													<button type="button" class="btn btn-secondary">ATS</button>
													<button type="button" class="btn btn-secondary">FS</button>
													<button type="button" class="btn btn-secondary">KS</button>
													<button type="button" class="btn btn-secondary">MGS</button>
													<button type="button" class="btn btn-secondary">MS</button>
													<button type="button" class="btn btn-secondary">PRS</button>
													<button type="button" class="btn btn-secondary">PS</button>
													<button type="button" class="btn btn-secondary">SHA</button>
													<button type="button" class="btn btn-secondary">ASS</button>
												</div>
											</div>
										</div>
										<div class='col'></div>
									</div>
									<div class='pt-gap'></div>
                                    <div class="tab-content"> 
                                        <div role="tabpanel" class="tab-pane fade show active" id="amulets">
											<?php foreach($items as $item){
													if(strtolower($item->category)=='amulets'){
											?>
												<div class="pt-feature-1">
														<div class="pt-feature-icon">
															<img class='shadowfilter' src="<?php echo base_url();?>assets/images/items/it<?php echo $item->code;?>.png" alt="">
														</div>
														<div class="pt-feature-cont">
														<strong class="text-main-6 "><?php echo $item->name;?></strong>
														<?php if(strlen($item->attributes)>0){ ?>
															
															<div class='columns'>
																<ul class="item-attributes item-attributes-main">
																	<?php echo $item->attributes;?>													
																</ul>
															</div>
														<?php }else{ echo "<br/>";} ?>
														<?php if(strlen($item->requirements)>0){ ?>
															<small class="text-info">Requirements</small>
															<div class='columns text-danger'>
																<ul class="item-attributes item-attributes-req">
																		<?php echo $item->requirements;?>															
																</ul>
															</div>
														<?php } ?>
														<?php if(strlen($item->spec)>0){ ?>
															<small class="text-success">Specialized</small>
															<div class='columns text-success'>
																<ul class="item-attributes item-attributes-spec">
																	<?php echo $item->spec;?>														
																</ul>
															</div>
														<?php } ?>
														</div>
													</div>
											<?php }} ?>	
                                        </div>
                                       
                                        <div role="tabpanel" class="tab-pane fade" id="rings">
											<?php foreach($items as $item){
													if(strtolower($item->category)=='rings'){
											?>
												<div class="pt-feature-1">
														<div class="pt-feature-icon">
															<img class='shadowfilter' src="<?php echo base_url();?>assets/images/items/it<?php echo $item->code;?>.png" alt="">
														</div>
														<div class="pt-feature-cont">
														<strong class="text-main-6 "><?php echo $item->name;?></strong>
														<?php if(strlen($item->attributes)>0){ ?>
															
															<div class='columns'>
																<ul class="item-attributes item-attributes-main">
																	<?php echo $item->attributes;?>													
																</ul>
															</div>
														<?php }else{ echo "<br/>";} ?>
														<?php if(strlen($item->requirements)>0){ ?>
															<small class="text-info">Requirements</small>
															<div class='columns text-danger'>
																<ul class="item-attributes item-attributes-req">
																		<?php echo $item->requirements;?>															
																</ul>
															</div>
														<?php } ?>
														<?php if(strlen($item->spec)>0){ ?>
															<small class="text-success">Specialized</small>
															<div class='columns text-success'>
																<ul class="item-attributes item-attributes-spec">
																	<?php echo $item->spec;?>														
																</ul>
															</div>
														<?php } ?>
														</div>
													</div>
											<?php }} ?>	
										</div>
										<div role="tabpanel" class="tab-pane fade" id="sheltoms">
											<?php foreach($items as $item){
													if(strtolower($item->category)=='sheltoms'){
											?>
												<div class="pt-feature-1">
														<div class="pt-feature-icon">
															<img class='shadowfilter' src="<?php echo base_url();?>assets/images/items/it<?php echo $item->code;?>.png" alt="">
														</div>
														<div class="pt-feature-cont">
														<strong class="text-main-6 "><?php echo $item->name;?></strong>
														<?php if(strlen($item->attributes)>0){ ?>
															
															<div class='columns'>
																<ul class="item-attributes item-attributes-main">
																	<?php echo $item->attributes;?>													
																</ul>
															</div>
														<?php }else{ echo "<br/>";} ?>
														<?php if(strlen($item->requirements)>0){ ?>
															<small class="text-info">Requirements</small>
															<div class='columns text-danger'>
																<ul class="item-attributes item-attributes-req">
																		<?php echo $item->requirements;?>															
																</ul>
															</div>
														<?php } ?>
														<?php if(strlen($item->spec)>0){ ?>
															<small class="text-success">Specialized</small>
															<div class='columns text-success'>
																<ul class="item-attributes item-attributes-spec">
																	<?php echo $item->spec;?>														
																</ul>
															</div>
														<?php } ?>
														</div>
													</div>
											<?php }} ?>	
										</div>
												
                                    </div>   
                                </div>

							</div><!--/pt-widget-content-->
						</div>


                <!--/maincontent-->
            </div>
            <!--sidebar-->
                <?php
                    $this->load->view('templates/sidebar');
                ?>
            <!--/sidebar-->

        </div>
    </div>
    <!--/container-->
